## This project is using Laravel's Framework

<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>


## Cara Install

- simpan source code dimana saja
- lakukan 'npm install'
- lakukan 'php artisan storage:link'
- buat database MySQL Maria DB dengan nama 'hmif'
- ubah file .env menjadi 

APP_NAME=HMIF
APP_ENV=local
APP_KEY=base64:QR9wjxHUpc3D3ciGo5UToZ60A9Qwev+uxe6+R+yI/uk=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=hmif
DB_USERNAME=root
DB_PASSWORD=

- lakukan 'php artisan migrate'
- lakukan 'php artisan db:seed'


Untuk memulai langsung saja gunakan 'php artisan serve'


## About This website

This site is intended for organizations called HMIF which have a role to help facilitate the delivery of news about the Informatics Engineering Student Association to the general public.

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

## Security Vulnerabilities

If you discover a security vulnerability within this project, please send an e-mail to Yusuf Fajar via [yufaza57@gmail.com](mailto:yufaza57@gmail.com). All security vulnerabilities will be promptly addressed.


