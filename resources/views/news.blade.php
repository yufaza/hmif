@extends('layouts.search-layout')

@section('content')
  @include('includes.date_helper')
  <div class="row">
  @foreach($news as $page)
  <div class="col s12 m6 l4 ">
    <div class="card small valign-wrapper">
      <div class="card-content" style="margin-left:auto; margin-right:auto">
        <h3 class="center-align hide-on-med-and-down light"><a href="{{route('page.single', ['slug' => $page->slug])}}" class="black-text">{{$page->title}}</a></h3>
        <h5 class="center-align hide-on-large-only"><a href="{{route('page.single', ['slug' => $page->slug])}}" class="black-text">{{$page->title}}</a></h5>
      
        
        <div class="center-align">
          {{indonesian_date($page->created_at)}}  
        </div>
      </div>
    </div></div>
  @endforeach</div>
  {{ $news->onEachSide(1)->links('includes.pagination-default') }}

@endsection