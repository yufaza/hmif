@extends('layouts.page-layout')

@section('styles')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
@endsection

@section('scripts')
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script>
  $(document).ready(function() {
    $('#example').DataTable( {
      "lengthChange": false,
      "info": false,
      "pageLength": 10,
      
        columnDefs: [
            {
                targets: [ 0, 1, 2 ],
                className: 'mdl-data-table__cell--non-numeric'
            }
        ]
    } );
} );
</script>
@endsection



@section('content')
  <div class="card">
    <div class="card-content">
      <h2 class="center hide-on-med-and-down"><b>{{$title}}</b></h2>
      <h5 class="center hide-on-large-only"><b>{{$title}}</b></h5>
      @include('includes.date_helper')
      <table id="example" class="mdl-data-table" style="width:100%">
        <thead>
          <tr>
              <th>Nama File</th>
              <th>Deskripsi</th>
              <th>Unduh</th>
          </tr>
        </thead>
        @foreach($files as $file)
            <tr>
                <td>{{$file->name}}</td>
                <td>{{$file->description}}</td>
                <td><a target="_blank" href="{{$file->featured}}">unduh</a></td>
            </tr>
        @endforeach
      </table>
    </div>
    <?php //echo $posts->render(); 
  echo $files->onEachSide(1)->links('includes.pagination-default');
  ?>
  </div>
@endsection