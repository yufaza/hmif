@extends('layouts.page-layout')

@section('styles')
@endsection

@section('scripts')
@endsection



@section('content')
  <div class="card">
    <div class="card-content">
      <h2 class="hide-on-med-and-down center"><b>{{$page->title}}</b></h2>
      <h5 class="hide-on-large-only center"><b>{{$page->title}}</b></h5>
      @include('includes.date_helper')
    </div>
    <div class="card-content" style="text-align:justify">
      <div id="post-content">
        {!!$page->content!!}
      </div>
    </div>
    <div class="card-content">
      <span class="small black-text"><i class="material-icons left">access_time</i>Dibuat pada: {{indonesian_date($page->created_at)}}</span>
    </div>
    
    @if($page->type != 'profile')
      @include('includes.next-prev-page')
    @endif
    @if($page->type == 'news')
    <div class="card-content">
      @include('includes.disqus-page')
    </div>
    @endif
  </div>
@endsection