@extends('layouts.article-layout')

@section('styles')
<style>
  aside {
    position: -webkit-sticky;
    position: sticky;
    top: 64px;
  }
  #main-container{
      width:100%
  }

  @media only screen and (min-width: 993px){
    #main-container{  
      width:90%
    }
    #main-nav .container{
      width:90%
    }
  }

  @media only screen and (min-width: 1367px){
    #main-container{  
      width:70%
    }
    #main-nav .container{
      width:70%
    }
  }

  @media only screen and (max-width: 993px){
    .card{
      box-shadow:none;
    }
  }

  .small-text{
    font-size:xx-small;
  }

  #post-content{
    margin-top:2em;
  }
</style>
@endsection

@section('scripts')
<script>
  $(document).ready(function(){
    $('.materialboxed').materialbox();
  });
</script>
<!-- Go to www.addthis.com/dashboard to customize your tools --> 
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5ba11c840fdf38ac"></script>
@endsection

@section('article')
  <div class="card">
    <div class="card-image">
      <img src="{{$post->featured}}" alt="{{$post->title}}" class="materialboxed">
    </div>
    <div class="card-content">
      <span class="badge left">Kategori: </span>
      <span class="new badge left green" data-badge-caption=" ">{{$post->category->name}}</span>
      <span class="badge left">Tag: </span>
      @foreach($post->tags as $tag)
      <span class="new badge left yellow darken-3" data-badge-caption=" ">
      <a href="" class="white-text">
        {{$tag->tag}}
      </a>
      </span>
      @endforeach
      <h2 class="flow-text"><b>{{$post->title}}</b></h2>
      @include('includes.date_helper')
      <span class="left">{{$post->user->name}}</span>
      <span class="badge left black-text"><i class="material-icons left">access_time</i>{{indonesian_date($post->created_at)}}</span>
      </span>
      <span class="right black-text">
        <i class="material-icons left">comment</i>
        <a href="/article/{{$post->slug}}#disqus_thread" class="black-text"></a>
      </span>
    </div>
    <div class="card-content" style="text-align:justify">
      <div class="addthis_inline_share_toolbox_tq7l center">
        <span class="left"><i class="material-icons left">share</i>Bagikan</span>
      </div>
      <div id="post-content">
        {!!$post->content!!}
      </div>
    </div>
    <div class="card-action cyan lighten-5">
      <div class="row">
        <div class="col s3">
          <img src="{{$post->user->profile->avatar}}" alt="{{$post->user->name}}" class="responsive-img" style="border-radius:50%">
        </div>
        <div class="col s9">
          <h6>Penulis: <b>{{$post->user->name}}</b></h6> 
          {{ $post->created_at->diffForHumans() }}
          <p class="flow-text">
          {!! $post->user->profile->about !!}
          </p>
        </div>
      </div>
    </div>
    @include('includes.next-prev-post')
    <div class="card-content">
      @include('includes.disqus')
    </div>
  </div>
@endsection