@extends('cms.layouts.app3')

@section('css')

@endsection

@section('scripts')

@endsection

@section('content-header')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Dashboard</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard v2</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <!-- Small boxes (Stat box) -->

 
    <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header">
              Edit Site Setting
            </div>
            <div class="card-body">
              <form action="{{ route('settings.update') }}" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                  <label for="site_name">Site Name</label>
                  <input type="text" name="site_name" value="{{ $settings->site_name }}" class="form-control">
                </div>
                <div class="form-group">
                  <label for="address">Address</label>
                  <input type="text" name="address" value="{{ $settings->address }}" class="form-control">
                </div>
                <div class="form-group">
                  <label for="contact_number">Contact phone</label>
                  <input type="text" name="contact_number" value="{{ $settings->contact_number }}" class="form-control">
                </div>
                <div class="form-group">
                  <label for="contact_email">Address</label>
                  <input type="email" name="contact_email" value="{{ $settings->contact_email }}" class="form-control">
                </div>
                <div class="form-group">
                  <label for="contact_email">Office</label>
                  <input type="test" name="office" value="{{ $settings->office }}" class="form-control">
                </div>
                <p>Last Updated: {{$settings->updated_at}}</p>
                
                <div class="form-group">
                  <div class="text-center">
                    <button class="btn btn-success" type="submit">Update site settings</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
    </div>

    </div>
    </div>
</section>
@endsection