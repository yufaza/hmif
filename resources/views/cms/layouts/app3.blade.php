<!DOCTYPE html>
<html lang="en">

<head>
    <style id="stndz-style"></style>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="Materialize is a modern responsive CSS framework based on Material Design by Google. ">
    <title>Pagination - Materialize</title>
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="images/favicon/apple-touch-icon-152x152.png">
    <meta name="msapplication-TileColor" content="#FFFFFF">
    <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
    <link rel="icon" href="images/favicon/favicon-32x32.png" sizes="32x32">
    <!--  Android 5 Chrome Color-->
    <meta name="theme-color" content="#EE6E73">
    <style>
        body {
            display: flex;
            min-height: 100vh;
            flex-direction: column;
        }

        main {
            flex: 1 0 auto;
        }
    </style>
    <!-- CSS-->
    <link href="https://materializecss.com/css/prism.css" rel="stylesheet">
    <link href="https://materializecss.com/css/ghpages-materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="https://fonts.googleapis.com/css?family=Inconsolata" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    @yield('css')
</head>

<body>
    @include('cms.includes.navbar')
    <main>
        @yield('content')
    </main>
    @include('cms.includes.footer')

    <!--  Scripts--> 
    <script src="https://materializecss.com/bin/materialize.js"></script>
    {{-- <script src="https://materializecss.com/docs/js/init.js"></script> --}}
    <script>
        M.AutoInit();
    </script>

    @yield('js')

    <div class="sidenav-overlay" style="display: none; opacity: 0;"></div>
</body>
</html>