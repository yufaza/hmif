  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="{{asset('dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Juragan</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="/cms/dashboard"  class="nav-link">
              <i class="fa fa-dashboard nav-icon"></i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="nav-header">Web Contents</li>
          <li class="nav-item">
            <a href="{{route('category.index')}}" class="nav-link">
              <i class="fa fa-folder-o nav-icon"></i>
              <p>Categories</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('tag.index')}}" class="nav-link">
              <i class="fa fa-tags nav-icon"></i>
              <p>Tags</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('post.index')}}" class="nav-link">
              <i class="fa fa-newspaper-o nav-icon"></i>
              <p>Posts</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('post.trashed')}}"  class="nav-link">
              <i class="fa fa-recycle nav-icon"></i>
              <p>Trashed Posts</p>
            </a>
          </li>
          
          @if(Auth::user()->admin)
          <!-- Managed by admin  -->
          <li class="nav-header">Site Managements</li>
            <li class="nav-item has-treeview">  
              <a href="#" class="nav-link">
                <i class="nav-icon fa fa-user"></i>
                <p>
                  User Management
                  <i class="fa fa-angle-left right"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{route('user.index')}}"  class="nav-link">
                    <i class="fa fa-users nav-icon"></i>
                    <p>Users</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{route('user.edit', ['user' => Auth::id()])}}"  class="nav-link">
                    <i class="fa fa-pencil-square nav-icon"></i>
                    <p>My Profiles</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item">  
              <a href="{{route('page.index')}}" class="nav-link">
                <i class="nav-icon fa fa-user"></i>
                <p>
                  Pages
                </p>
              </a>
            </li>
            <li class="nav-item">  
              <a href="{{route('file.index')}}" class="nav-link">
                <i class="nav-icon fa fa-user"></i>
                <p>
                  Files
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{route('settings')}}"  class="nav-link">
                <i class="fa fa-cog nav-icon"></i>
                <p>About</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{route('image.index')}}"  class="nav-link">
                <i class="fa fa-cog nav-icon"></i>
                <p>Background-Images</p>
              </a>
            </li>
          </li>
          <li class="nav-header">Unfinished</li>
            <li class="nav-item has-treeview">  
              <a href="#" class="nav-link">
                <i class="nav-icon fa fa-money"></i>
                <p>
                  Financial
                  <i class="fa fa-angle-left right"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href=""  class="nav-link">
                    <i class="fa fa-money nav-icon"></i>
                    <p>Kas</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href=""  class="nav-link">
                    <i class="fa fa-money nav-icon"></i>
                    <p>Uang Bersama</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item has-treeview">  
              <a href="#" class="nav-link">
                <i class="nav-icon fa fa-envelope"></i>
                <p>
                  Mail
                  <i class="fa fa-angle-left right"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href=""  class="nav-link">
                    <i class="fa fa-envelope nav-icon"></i>
                    <p>Mail</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href=""  class="nav-link">
                    <i class="fa fa-circle-o nav-icon"></i>
                    <p>extra</p>
                  </a>
                </li>
              </ul>
            </li>
          </li>
          @endif
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>