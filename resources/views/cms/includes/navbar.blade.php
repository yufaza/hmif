
    <header>
      <nav class="top-nav">
          <div class="container">
              <div class="nav-wrapper">
                  <div class="row">
                      <div class="col s12 m10 offset-m1">
                          <h1 class="header">{{isset($title)? $title : "HMIF" }}</h1>
                      </div>
                  </div>
              </div>
          </div>
      </nav>
      <div class="container">
        <a href="#" data-target="nav-mobile" class="top-nav sidenav-trigger full hide-on-large-only">
          <i class="material-icons">menu</i>
        </a>
      </div>
      <ul id="nav-mobile" class="sidenav sidenav-fixed" style="transform: translateX(0%);">
          <li class="logo">
            <a id="logo-container" href="/" class="brand-logo">
              <img src="/storage/background_images/logo.png" alt="HMIF" class="hmif-logo-small" style="height:12vh; margin-top:5vh"/>
            </a>
          </li>
         
          <li id="Dashboard" class="bold"><a href="about.html" class="waves-effect waves-teal">Dashboard</a></li>
          <li id="Categories" class="bold"><a href="about.html" class="waves-effect waves-teal">Categories</a></li>
          <li id="Tags" class="bold"><a href="about.html" class="waves-effect waves-teal">Tags</a></li>
          <li id="Posts" class="bold"><a href="/cms/post" class="waves-effect waves-teal">Posts</a></li>
          <li id="Trashed-Post" class="bold"><a href="about.html" class="waves-effect waves-teal">Trashed Posts</a></li>
          <li id="Pages" class="bold"><a href="about.html" class="waves-effect waves-teal">Pages</a></li>
          <li id="Files" class="bold"><a href="about.html" class="waves-effect waves-teal">Files</a></li>
          <li id="Users" class="bold"><a href="about.html" class="waves-effect waves-teal">Users</a></li>
          <li id="About" class="bold"><a href="about.html" class="waves-effect waves-teal">About</a></li>
          <li id="Background-Images" class="bold"><a href="about.html" class="waves-effect waves-teal">Background Images</a></li>
          <li class="no-padding">
              <ul class="collapsible collapsible-accordion">
                  <li class="bold"><a class="collapsible-header waves-effect waves-teal" tabindex="0">CSS</a>
                      <div class="collapsible-body">
                          <ul>
                              <li><a href="color.html">Color</a></li>
                              <li><a href="grid.html">Grid</a></li>
                              <li><a href="helpers.html">Helpers</a></li>
                              <li><a href="madia-css.html">Media</a></li>
                              <li><a href="pulse.html">Pulse</a></li>
                              <li><a href="sass.html">Sass</a></li>
                              <li><a href="shadow.html">Shadow</a></li>
                              <li><a href="table.html">Table</a></li>
                              <li><a href="css-transitions.html">Transitions</a></li>
                              <li><a href="typography.html">Typography</a></li>
                          </ul>
                      </div>
                  </li>
              </ul>
          </li>
          <li class="bold"><a href="mobile.html" class="waves-effect waves-teal">Mobile</a></li>
      </ul>
      <!-- Sidebar BSA-->
      <script src="//m.servedby-buysellads.com/monetization.js" type="text/javascript"></script>
      <div class="bsa-cpc"></div>
      <script>
          (function () {
              if (typeof _bsa !== 'undefined' && _bsa) {
                  _bsa.init('default', 'CKYD55QM', 'placement:materializecsscom', {
                      target: '.bsa-cpc',
                      align: 'horizontal',
                      disable_css: 'true'
                  });
              }
          })();
      </script>
      <div class="patreon-ad"><a href="https://www.patreon.com/materialize" target="_blank" class="waves-effect">Become
              a Patron</a></div>
  </header>