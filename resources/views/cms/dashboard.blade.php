@extends('cms.layouts.app3')

@section('content')
<div class="row">
    <ul class="tabs">
        <li class="tab col s3"><a href="#test1">Test 1</a></li>
        <li class="tab col s3"><a class="active" href="#test2">Test 2</a></li>
        <li class="tab col s3 disabled"><a href="#test3">Disabled Tab</a></li>
        <li class="tab col s3"><a href="#test4">Test 4</a></li>
    </ul>
    <div id="test1" class="container">
        <div class="row">

            <div class="col s12 m12 offset-m1 xl7 offset-xl1">

                <!--  Collections Section  -->
                <div id="basic" class="section scrollspy">

                    <div class="row">
                        <div class="col s12">
                            <p class="caption">Add pagination links to help split up your long content into
                                shorter, easier to understand blocks.</p>
                            <h3 class="header">Basic</h3>

                        </div>
                    </div>
                    <!-- End collections -->


                </div>

            </div>
        </div>
        <div id="test2" class="container">Test 2</div>
        <div id="test3" class="container">Test 3</div>
        <div id="test4" class="container">Test 4</div>
    </div>
</div>
@endsection