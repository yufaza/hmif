@extends('cms.layouts.app3')

@section('css')

@endsection

@section('scripts')
<script>
  $(function () {
    $("#example2").DataTable();
    $('#categories-table').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>
@endsection

@section('content-header')
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v2</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
@endsection

@section('content')
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <a href="{{route('image.create')}}" class="btn btn-success"><i class="nav-icon fa fa-plus-square mr-2"></i>Add Image</a>
            </div>
          </div>
        </div>
      </div> 
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">List of Image</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive">
              <table id="categories-table" class="table table-bordered table-striped ">
                <thead>
                  <tr>
                    <th>Image Name</th>
                    <th>Visual</th>
                    <th>Last updated</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($images as $image)
                    <tr>
                      <td>{{$image->name}}</td>
                      <td><img src="/storage/{{$image->featured}}" alt="{{$image->name}}" width="160px"></td>
                      <td>{{$image->created_at}}</td>
                      <td>
                        <form action="{{route('image.destroy', ['image' => $image->id])}}" method="post">
                          @method('DELETE')
                          {{csrf_field()}}
                          <div class="btn-group-vertical w-100">
                            <a href="{{route('image.edit', ['image' => $image->id])}}" class="btn btn-primary btn-sm"><i class="nav-icon fa fa-pencil-square-o mr-2"></i>Edit</a>
                            <button type="submit" class="btn btn-danger btn-sm"><i class="nav-icon fa fa-trash-o mr-2"></i>Delete</button>
                          </div>
                        </form>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <th>Image Name</th>
                    <th>Visual</th>
                    <th>Last updated</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection