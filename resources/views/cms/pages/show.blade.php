:640px" /></a></div>

<p><br />
    Apa itu diagram Use Case? Mengapa kita menggunakan diagram Use Case? Pertanyaan tersebut sering kali muncul dalam
    mesin pencari di internet. Beberapa orang tidak tahu apa itu use case, sedangkan sisanya kurang memperkirakan
    kegunaan use case dalam mengembangkan produk software yang bagus. Lantas apakah use case dianggap tidak penting?
    berikut akan saya sedikit paparkan pengetahuan saya mengenai diagram use case.<br />
    <br />
    Jadi apa itu diagram Use Case? UML use case diagram adalah bentuk utama dari persyaratan sistem / perangkat lunak
    untuk program perangkat lunak baru yang sedang dikembangkan. Use case menentukan perilaku yang diharapkan (apa),
    dan bukan metode yang tepat untuk mewujudkannya (bagaimana). Kasus penggunaan sekali ditentukan dapat dilambangkan
    baik representasi tekstual dan visual (seperti UML). Konsep kunci dari pemodelan use case adalah membantu kita
    mendesain sistem dari perspektif pengguna akhir(end-user). Ini adalah salah satu teknik yang efektif untuk
    mengkomunikasikan perilaku sistem dalam istilah pengguna dengan menentukan semua perilaku sistem yang terlihat
    secara eksternal.<br />
    <br />
    Diagram use case biasanya sederhana. Itu tidak menunjukkan detail dari kasus penggunaan:<br />
    <br />
    Diagram use case meringkas beberapa hubungan antara kasus penggunaan, aktor, dan sistem.<br />
    Juga tidak menunjukkan urutan langkah-langkah yang dilakukan untuk mencapai tujuan dari setiap use case.<br />
    Seperti yang dikatakan sebelumnya, diagram use case harus sederhana dan hanya berisi beberapa bentuk saja. Jika
    diagram usecase Anda memiliki lebih dari 20 usecase, mungkin Anda keliru dalam menggunakan diagram usecase.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<h2>Komponen Use case diagram</h2>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<table border="1" cellpadding="1" cellspacing="1">
    <thead>
        <tr>
            <th scope="col">Istilah dan Definisi</th>
            <th scope="col">Simbol</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>&nbsp;Actor (Aktor)
                <ul>
                    <li>Adalah orang atau sistem yang memberi dan memperoleh manfaat dalam sistem.</li>
                    <li>Dapat dikaitkan dengan aktor lain oleh asosiasi spesialisasi /superclass,</li>
                    <li>Dilambangkan dengan panah dengan panah kosong.</li>
                    <li>Ditempatkan di luar batas sistem.</li>
                </ul>
            </td>
            <td>
                <div class="separator" style="clear:both; text-align:center"><a href="https://3.bp.blogspot.com/-8vR8yrpKKB4/XBOztTcFJYI/AAAAAAAAAWY/uyFLZkZp5-kTlW-UQ71SPd9PnPDh3EfqwCEwYBhgL/s1600/Aktor.png"
                        style="margin-left: 1em; margin-right: 1em;"><img src="https://3.bp.blogspot.com/-8vR8yrpKKB4/XBOztTcFJYI/AAAAAAAAAWY/uyFLZkZp5-kTlW-UQ71SPd9PnPDh3EfqwCEwYBhgL/s1600/Aktor.png" /></a></div>
            </td>
        </tr>
        <tr>
            <td>&nbsp;Use Case
                <ul>
                    <li>Merupakan bagian utama dari sistem fungsionalitas.</li>
                    <li>Dapat memperpanjang kasus penggunaan lain.</li>
                    <li>Dapat menggunakan kasus penggunaan lain.</li>
                    <li>Ditempatkan di dalam batas sistem.</li>
                    <li>Dilabeli dengan frasa kata kerja-kata benda.</li>
                </ul>
            </td>
            <td>
                <div class="separator" style="clear:both; text-align:center"><a href="https://2.bp.blogspot.com/-Rrn5in5mFOE/XBOzuvwc87I/AAAAAAAAAWc/tvDbcCIDhIg-SZXj8y2cxMDQbYfKlYrDgCLcBGAs/s1600/Nama%2BUse%2BCase.png"
                        style="margin-left: 1em; margin-right: 1em;"><img src="https://2.bp.blogspot.com/-Rrn5in5mFOE/XBOzuvwc87I/AAAAAAAAAWc/tvDbcCIDhIg-SZXj8y2cxMDQbYfKlYrDgCLcBGAs/s1600/Nama%2BUse%2BCase.png" /></a></div>
            </td>
        </tr>
        <tr>
            <td>&nbsp;A System Boundary (Batasan Sistem)
                <ul>
                    <li>&nbsp;</li>
                    <li>Memiliki label nama sistem di dalam atau di atas simbol.</li>
                    <li>Mewakili ruang lingkup sistem.</li>
                </ul>
            </td>
            <td>
                <div class="separator" style="clear:both; text-align:center"><a href="https://4.bp.blogspot.com/-h87B13e2aT8/XBOzug9grOI/AAAAAAAAAWg/NN1z5dg31i8s9F40iTa6YxfxIVxy7RlogCLcBGAs/s1600/Nama%2BSistem.png"
                        style="margin-left: 1em; margin-right: 1em;"><img src="https://4.bp.blogspot.com/-h87B13e2aT8/XBOzug9grOI/AAAAAAAAAWg/NN1z5dg31i8s9F40iTa6YxfxIVxy7RlogCLcBGAs/s1600/Nama%2BSistem.png" /></a></div>
            </td>
        </tr>
        <tr>
            <td><span style="background-color:white; font-family:&quot;roboto&quot;; font-size:16px">Communication Link</span>&nbsp;(Tautan
                Komunikasi)
                <ul>
                    <li>Mengaitkan aktor dengan use case yang berinteraksi.</li>
                </ul>
            </td>
            <td>
                <div class="separator" style="clear:both; text-align:center"><a href="https://3.bp.blogspot.com/-qlSs66xunUQ/XBOzu-EsmzI/AAAAAAAAAWk/JB6y4ZnsnnIKKjobWNyxNkVTBhpub2cwQCLcBGAs/s1600/garis.png"
                        style="margin-left: 1em; margin-right: 1em;"><img src="https://3.bp.blogspot.com/-qlSs66xunUQ/XBOzu-EsmzI/AAAAAAAAAWk/JB6y4ZnsnnIKKjobWNyxNkVTBhpub2cwQCLcBGAs/s1600/garis.png" /></a></div>
            </td>
        </tr>
    </tbody>
</table>

<p>&nbsp;</p>

<h4>Aktor</h4>

<p>Gambar stickman berlabel pada diagram mewakili aktor. Seorang aktor mirip dengan entitas eksternal yang ditemukan
    dalam DFD &mdash; itu adalah orang atau sistem lainnya yang berinteraksi dengan dan mendapatkan nilai dari sistem.
    Seorang aktor tidak mewakili pengguna spesifik, tetapi peran yang dapat dimainkan pengguna saat berinteraksi dengan
    sistem.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<h4>Use Case</h4>

<p>Sebuah use case, digambarkan oleh oval, adalah proses besar yang akan dilakukan oleh sistem. Usecase selalu dilabeli
    dengan frasa kata kerja-kata benda.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<h4>Batas Sistem</h4>

<p>Usecase berada didalam batas sistem, yaitu kotak yang mewakili sistem dan dengan jelas&nbsp; menggambarkan bagian
    mana dari diagram bersifat eksternal atau internal.&nbsp; Nama sistem dapat muncul baik di dalam atau di atas
    kotak.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<h4>Tautan Komunikasi</h4>

<p>Use Case terhubung ke aktor atau ke Use Case yang lain melalui Tautan Komunikasi. Garis yang ditarik dari seorang
    aktor ke use case menggambarkan sebuah asosiasi. Asosiasi biasanya mewakili komunikasi dua arah antara use case dan
    aktor. Selain Asosiasi ada juga Include, Extend, dan Generalization.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<h2>Contoh Diagram Use Case</h2>

<p>&nbsp;</p>

<p>&nbsp;</p>

<div class="separator" style="clear:both; text-align:center"><a href="https://1.bp.blogspot.com/-ZRNjeO-ehFo/XBPJHOEqlJI/AAAAAAAAAXs/NtKeLJsF_NcuZxTkXL36U0pK3TL6tm-tACLcBGAs/s1600/Diagram%2Buse%2Bcase.png"
        style="margin-left: 1em; margin-right: 1em;"><img src="https://1.bp.blogspot.com/-ZRNjeO-ehFo/XBPJHOEqlJI/AAAAAAAAAXs/NtKeLJsF_NcuZxTkXL36U0pK3TL6tm-tACLcBGAs/s1600/Diagram%2Buse%2Bcase.png" /></a></div>

<div class="separator" style="clear:both; text-align:center">&nbsp;</div>

<div class="separator" style="clear:both; text-align:center">&nbsp;</div>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<h2>Use Case Scenario</h2>

<p>Use Case scenario merupakan penjelasan secara tekstual dari sekumpulan skenario interaksi. Setiap skenario
    mendeskripsikan urutan aksi/langkah yang dilakukan aktor ketika berinteraksi dengan sistem, baik yang berhasil
    maupun gagal.</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<h2>Contoh Use Case Scenario</h2>

<p>&nbsp;</p>

<p>&nbsp;</p>

<table border="1" cellpadding="1" cellspacing="1">
    <tbody>
        <tr>
            <td colspan="2">Use Case Order Produk</td>
        </tr>
        <tr>
            <td>Tujuan</td>
            <td>Memberikan Surat pemesanan barang kepada perusahaan</td>
        </tr>
        <tr>
            <td>Aktor</td>
            <td>Customer</td>
        </tr>
        <tr>
            <td>Kondisi Awal</td>
            <td>Customer menjalin hubungan dengan perusahaan</td>
        </tr>
        <tr>
            <td>Skenario Utama</td>
            <td>
                <ol>
                    <li>Customer membuat daftar barang yang akan dibeli dari perusahaan</li>
                    <li>Customer menentukan waktu produksi yang diinginkan</li>
                    <li>Custumer membuat surat pemesanan</li>
                    <li>Customer mengirimkan surat pemesanan kepada perusahaan secara langsung/melalui email</li>
                </ol>
            </td>
        </tr>
        <tr>
            <td>Kondisi Akhir</td>
            <td>Surat pemesanan diterima oleh Perusahaan</td>
        </tr>
    </tbody>
</table>

<p>&nbsp;</p>

<table border="1" cellpadding="1" cellspacing="1">
    <tbody>
        <tr>
            <td colspan="2">Use Case Menyanggupi Order</td>
        </tr>
        <tr>
            <td>Tujuan</td>
            <td>Menyanggupi serta mempersiapkan pesanan dari Customer</td>
        </tr>
        <tr>
            <td>Aktor</td>
            <td>Sales, Teknisi Workshop</td>
        </tr>
        <tr>
            <td>Kondisi Awal</td>
            <td>Perusahaan menerima surat pemesanan dari customer</td>
        </tr>
        <tr>
            <td>Skenario Utama</td>
            <td>
                <ol>
                    <li>Sales meneruskan surat pemesanan kepada Teknisi Workshop</li>
                    <li>Teknisi Workshop mengecek ketersediaan barang</li>
                    <li>Teknisi Worshop mengkonfirmasi ketersediaan barang&nbsp;kepada sales secara langsung/tidak
                        langsung</li>
                </ol>
            </td>
        </tr>
        <tr>
            <td>Skenario Alternatif</td>
            <td>
                <ol>
                    <li>Jika produk barang tidak tersedia namun bahan pembuatnya, teknisi serta waktu pembuatannya
                        mencukupi. maka Teknisi menyanggupi pesanan.</li>
                    <li>Jika produk benar-benar tidak tersedia dan tidak dapat dibuat sesuai target waktu. maka Teknisi
                        akan menghubungi Sales secara langsung/tidak langsung&nbsp;agar pesanan dari
                        Customer&nbsp;ditolak.</li>
                </ol>
            </td>
        </tr>
        <tr>
            <td>Kondisi Akhir</td>
            <td>Produk disiapkan oleh Teknisi</td>
        </tr>
    </tbody>
</table>

<p>&nbsp;</p>

<p>&nbsp;</p>

<table border="1" cellpadding="1" cellspacing="1">
    <tbody>
        <tr>
            <td colspan="2">Use Case Mengirim Produk</td>
        </tr>
        <tr>
            <td>Tujuan</td>
            <td>Mengirimkan Produk kepada Customer</td>
        </tr>
        <tr>
            <td>Aktor</td>
            <td>Sales, Teknisi Workshop</td>
        </tr>
        <tr>
            <td>Kondisi Awal</td>
            <td>Produk telah disiapkan oleh Teknisi Workshop</td>
        </tr>
        <tr>
            <td>Skenario Utama</td>
            <td>&nbsp;
                <ol>
                    <li>Sales membuat dan mencetak surat tagihan dan surat jalan.</li>
                    <li>Sales memberikan surat tagihan dan surat jalan kepada Teknisi yang mengirimkan produk.</li>
                    <li>Teknisi Workshop&nbsp;mengirimkan produk kepada Customer menggunakan kendaraan operasional.</li>
                    <li>Teknisi Workshop menyerahkan produk serta surat-surat kepada Customer.</li>
                </ol>
            </td>
        </tr>
        <tr>
            <td>Kondisi Akhir</td>
            <td>Produk, Surat tagihan serta Surat jalan diterima oleh Customer</td>
        </tr>
    </tbody>
</table>

<p>&nbsp;</p>

<p>&nbsp;</p>

<table border="1" cellpadding="1" cellspacing="1">
    <tbody>
        <tr>
            <td colspan="2">Use Case Konfirmasi Pembayaran</td>
        </tr>
        <tr>
            <td>Tujuan</td>
            <td>Membayar Produk yang dibeli dari perusahaan</td>
        </tr>
        <tr>
            <td>Aktor</td>
            <td>Customer</td>
        </tr>
        <tr>
            <td>Kondisi Awal</td>
            <td>Produk, Surat tagihan serta Surat jalan diterima oleh Customer</td>
        </tr>
        <tr>
            <td>Skenario Utama</td>
            <td>&nbsp;
                <ol>
                    <li>Customer membayar sejumlah uang yang tertera dalam surat tagihan kedalam rekening bank milik
                        perusahaan</li>
                </ol>
            </td>
        </tr>
        <tr>
            <td>Kondisi Akhir</td>
            <td>Terjadi transfer dana dari Customer kepada Perusahaan</td>
        </tr>
    </tbody>
</table>

<p>&nbsp;</p>

<p>&nbsp;</p>

<table border="1" cellpadding="1" cellspacing="1">
    <tbody>
        <tr>
            <td colspan="2">Use Case Membuat Laporan Penjualan</td>
        </tr>
        <tr>
            <td>Tujuan</td>
            <td>Membuat Laporan Penjualan</td>
        </tr>
        <tr>
            <td>Aktor</td>
            <td>Sales</td>
        </tr>
        <tr>
            <td>Kondisi Awal</td>
            <td>Telah terjadi beberapa penjualan produk</td>
        </tr>
        <tr>
            <td>Skenario Utama</td>
            <td>&nbsp;
                <ol>
                    <li>Sales mengumpulkan dokumen yang berhubungan denga penjualan.</li>
                    <li>Sales mengelompokkan dokumen sesuai dengan Customer, Waktu, atau Produk sesuai kebutuhan.</li>
                    <li>Sales melakukan pengolahan dokumen penjualan dalam aplikasi spreadsheet sehingga tercipta
                        informasi penjualan.</li>
                    <li>Sales memasukkan informasi&nbsp;tersebut kedalam laporan penjualan.</li>
                </ol>
            </td>
        </tr>
        <tr>
            <td>Kondisi Akhir</td>
            <td>Sales membuat laporan penjulan</td>
        </tr>
    </tbody>
</table>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<h2>Extra</h2>

<p>Kapan kita harus menggunakan diagram usecase?<br />
    Diagram use case tidak memberikan banyak detail, jangan berharap untuk memodelkan urutan di mana langkah-langkah
    dilakukan. Sebaliknya, diagram use case yang tepat menggambarkan gambaran tingkat tinggi dari hubungan antara kasus
    penggunaan, aktor, dan sistem. Para ahli merekomendasikan bahwa diagram use case digunakan untuk melengkapi kasus
    penggunaan tekstual yang lebih deskriptif.<br />
    <br />
    UML Use Case Diagram ideal untuk:</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<ol>
    <li>Mewakili tujuan interaksi sistem-pengguna</li>
    <li>Mendefinisikan dan mengatur persyaratan fungsional dalam suatu sistem</li>
    <li>Menentukan konteks dan persyaratan sistem</li>
    <li>Memodelkan aliran dasar peristiwa dalam kasus penggunaan</li>
</ol>

<p>&nbsp;
    <p><br />
        sumber :</p>
</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<p>&nbsp;</p>

<ul>
    <li>Alan Dennis, Barbara H Wixom, Roberta M. Roth System Analysis and Design 6th edition</li>
    <li>Tri A. Kurniawan.2018.Pemodelan Use Case (UML): Evaluasi Terhadap beberapa Kesalahan Dalam Praktik.</li>
    <li>https://www.uml-diagrams.org/use-case-diagrams.html</li>
    <li>https://www.lucidchart.com/pages/uml-use-case-diagram</li>
</ul>
