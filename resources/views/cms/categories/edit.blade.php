@extends('cms.layouts.app3')

@section('css')

@endsection

@section('scripts')

@endsection

@section('content-header')
    @include('cms.includes.validation-errors')
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v2</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
@endsection

@section('content')
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body d-flex">
                        <a href="{{ URL::previous() }}" class="btn btn-secondary mr-auto"><i class="fa fa-arrow-circle-left mr-2"></i>Back</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-header">
                Update category: {{ $category->name }}
              </div>
              <div class="card-body">
                <form action="{{ route('category.update', ['id' => $category->id]) }}" method="post">
                  @method('PUT')
                  {{ csrf_field() }}
                  <div class="form-group">
                    <label for="name">Title</label>
                    <input type="text" name="name" value="{{ $category->name }}" class="form-control">
                  </div>
                  
                  <div class="form-group">
                    <div class="text-center">
                      <button class="btn btn-success" type="submit">Edit Category</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
@endsection