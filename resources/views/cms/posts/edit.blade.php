@extends('cms.layouts.app3')

@section('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href='{{asset('css/froala_editor.pkgd.min.css')}}' rel='stylesheet' type='text/css' />
<link href='{{asset('css/froala_style.min.css')}}' rel='stylesheet' type='text/css' />
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

@endsection

@section('js')

<script type='text/javascript' src='{{asset('js/froala_editor.pkgd.min.js')}}'></script>
<script>
    $(document).ready(function() {
        $('#summernote').froalaEditor();
    });
</script>
<script>
    $(document).ready(function() {
        $('#posts-table').DataTable({
            "scrollX": true,
            "lengthChange": false
        });
    } );
</script>
@endsection

@section('content')
@include('cms.includes.validation-errors')
<div class="row">
    <div class="col s12">
        <div class="container">
            <div class="card">
                <div class="card-content">
                    <a href="{{ URL::previous() }}" class="waves-effect waves-light btn-small"><i class="fa fa-arrow-circle-left mr-2"></i>Back</a>
                    <form action="{{route('post.destroy', ['post' => $post->id])}}" class="right" method="POST">
                        <a href="{{route('post.edit', ['post' => $post->id])}}" class="waves-effect waves-light btn-small"><i
                                class="fa fa-pencil-square-o mr-2"></i>Edit Post</a>
                        <button type="submit" class="waves-effect waves-light btn-small"><i class="nav-icon fa fa-trash-o mr-2"></i>Trash</button>
                        @method('DELETE')
                        {{csrf_field()}}
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col s12">
        <div class="container">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <h3 class="header">{{'Edit :'.$post->title}}</h3>
            <form action="{{ route('post.update', ['post' => $post->id]) }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="input-field col s12">
                    {{-- <i class="material-icons prefix">textsms</i> --}}
                <input type="text" name="title" id="title" value="{{$post->title}}">
                    <label for="title">Title</label>
                </div>
                  <div class="file-field input-field col s12">
                      <div class="btn">
                          <span>Featured Image</span>
                          <input type="file" name="featured" id="featured">
                      </div>
                      <div class="file-path-wrapper">
                          <input class="file-path validate" type="text">
                      </div>
                  </div>
                  <div class="input-field col s12">
                      <select name="category_id">
                          <option value="" disabled selected>Select a Category</option>
                          @foreach($categories as $category)
                          <option value="{{ $category->id }}" 
                              @if($post->category_id == $category->id)
                              selected
                              @endif
                          >{{ $category->name }}</option>
                          @endforeach
                      </select>
                      <label>Category</label>
                  </div>
                  <div class="input-field col s12">
                      <p><label>
                              Tags
                          </label></p>
                      @foreach($tags as $tag)
                      <p>
                          <label>
                              <input type="checkbox" class="filled-in" value="{{ $tag->id }}" name="tags[]"
                                @foreach($post->tags as $t)
                                  @if($tag->id == $t->id)
                                    checked
                                  @endif
                                @endforeach
                              >
                              <span>{{ $tag->tag }}</span>
                          </label>
                      </p>
                      @endforeach
                  </div>

                  <div class="input-field col s12">
                    <textarea name="content" id="summernote">
                      {!!$post->content!!}
                    </textarea>
                      <label for="summernote">Content</label>
                  </div>
                  <div class="input-field col s12">
                      <button class="btn waves-effect waves-light" type="submit" name="action">Submit
                          <i class="material-icons right">send</i>
                      </button>
                      <div>
                          <script async src="https://imgbb.com/upload.js" data-palette="blue" data-sibling-pos="before"></script>
                      </div>
                  </div>
            </form>
        </div>
    </div>
</div>
@endsection