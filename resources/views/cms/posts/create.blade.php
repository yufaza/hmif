@extends('cms.layouts.app3')

@section('css')

@endsection

@section('scripts')
<script src="//cdn.ckeditor.com/4.11.1/full/ckeditor.js"></script>
<script>
   CKEDITOR.replace( 'content' );
   CKEDITOR.config.height = 500;
</script>
@endsection

@section('content-header')
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v2</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
@endsection

@section('content')
    @include('cms.includes.validation-errors')
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body d-flex">
                        <a href="{{ URL::previous() }}" class="btn btn-secondary mr-auto"><i class="fa fa-arrow-circle-left mr-2"></i>Back</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-header">
                Create a new post
              </div>
              <div class="card-body">
                <form action="{{ route('post.store') }}" method="post" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" name="title" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="featured">Featured image</label>
                    <input type="file" name="featured" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="category">select a category</label>
                    <select name="category_id" id="category" class="form-control">
                      @foreach($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="tags">Select tags</label>
                    @foreach($tags as $tag)
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <div class="input-group-text">
                              <input type="checkbox" value="{{ $tag->id }}" name="tags[]"> 
                            </div>
                          </div>
                          <input type="text" disabled class="form-control" value="{{ $tag->tag }}">
                        </div>
                    @endforeach
                  </div>
                  <div class="form-group">
                    <label for="content">Content</label>
                    <div class="right">
                      <script async src="https://imgbb.com/upload.js" data-palette="blue" data-sibling-pos="before"></script>
                    </div>
                    <textarea name="content" id="content" rows="60" class="form-control"></textarea>
                  </div>
                  <div class="form-group">
                    <div class="text-center">
                      <button class="btn btn-success" type="submit">Store Post</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
@endsection