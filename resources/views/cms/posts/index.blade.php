@extends('cms.layouts.app3')

@section('css')

@endsection

@section('scripts')
<script>
  $(function () {
    $("#example2").DataTable();
    $('#posts-table').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>
@endsection

@section('content-header')
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v2</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
@endsection

@section('content')
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <a href="{{route('post.create')}}" class="btn btn-success"><i class="nav-icon fa fa-plus-square mr-2"></i>Add Post</a>
              </div>
            </div>
          </div>
        </div> 
        <div class="row">
          <div class="col-lg-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">List of Posts</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive">
              <table id="posts-table" class="table table-bordered table-striped ">
                <thead>
                <tr>
                  <th>Featured Image</th>
                  <th>Title</th>
                  <th>Author</th>
                  <th>Tags</th>
                  <th>Creation Date</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($posts as $post)
                <tr>
                  <td><img src="{{$post->featured}}" alt="{{$post->title}}" width="160px" height="90px"></td>
                  
                  <td>{{$post->title}}</td>
                  <td>{{$post->user->name}}</td>
                  <td>
                    @foreach($post->tags as $tag)
                      <span class="badge badge-info">{{$tag->tag." "}}</span>
                    @endforeach
                  </td>
                  <td>{{$post->created_at}}</td>
                  <td>
                    <form action="{{route('post.destroy', ['post' => $post->id])}}" method="POST">
                      @method('DELETE')
                      {{csrf_field()}}
                      <div class="btn-group-vertical w-100">
                        <a href="{{route('post.show', ['post' => $post->id])}}" class="btn btn-success btn-sm"><i class="nav-icon fa fa-eye mr-2"></i>View</a>
                        <a href="{{route('post.edit', ['post' => $post->id])}}" class="btn btn-primary btn-sm"><i class="nav-icon fa fa-pencil-square-o mr-2"></i>Edit</a>
                        <button type="submit" class="btn btn-danger btn-sm"><i class="nav-icon fa fa-trash-o mr-2"></i>Trash</button>
                      </div>
                    </form>
                  </td>
                </tr>
                @endforeach

                </tbody>
                <tfoot>
                <tr>
                  <th>Featured Image</th>
                  <th>Title</th>
                  <th>Author</th>
                  <th>Tags</th>
                  <th>Creation Date</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection