@extends('cms.layouts.app3')

@section('css')

@endsection

@section('scripts')
<script>
  $(function () {
    $("#example2").DataTable();
    $('#posts-table').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "info": true,
      "autoWidth": false,
      "columnDefs": [
          { targets: [0,4], orderable: false },
          { targets: [1,2,3], orderable: true}
      ]
    });
  });
</script>
@endsection

@section('content-header')
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v2</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
@endsection

@section('content')
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">List of Posts</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive">
              <table id="posts-table" class="table table-bordered table-striped ">
                <thead>
                <tr>
                  <th>Featured Image</th>
                  <th>Title</th>
                  <th>Author</th>
                  <th>Category</th>
                  <th>Tags</th>
                  <th>Creation Date</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($posts as $post)

                <tr>
                  <td><img src="{{$post->featured}}" alt="{{$post->title}}" width="160px" height="90px"></td>
                  <td>{{$post->title}}</td>
                  <td>{{($post->user) ? $post->user->name : 'tidak ada'}}</td>
                  <td>{{($post->category) ? $post->category->name : 'tidak ada'}}</td>
                  <td>
                    @foreach($post->tags as $tag)
                      <span class="badge badge-info">{{$tag->tag." "}}</span>
                    @endforeach
                  </td>
                  <td>{{$post->created_at}}</td>
                  <td>
                    <div class="btn-group-vertical w-100">
                      <a href="{{route('post.restore', ['post' => $post->id])}}" class="btn btn-success btn-sm"><i class="nav-icon fa fa-recycle mr-2"></i>Restore</a>
                        <a href="{{route('post.edit', ['post' => $post->id])}}" class="btn btn-primary btn-sm"><i class="nav-icon fa fa-pencil-square-o mr-2"></i>Edit</a>
                      <a href="{{route('post.kill', ['post' => $post->id])}}" class="btn btn-danger btn-sm"><i class="nav-icon fa fa-remove mr-2"></i>Delete</a>
                    </div>
                  </td>
                </tr>
                @endforeach

                </tbody>
                <tfoot>
                <tr>
                  <th>Featured Image</th>
                  <th>Title</th>
                  <th>Author</th>
                  <th>Category</th>
                  <th>Tags</th>
                  <th>Creation Date</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection