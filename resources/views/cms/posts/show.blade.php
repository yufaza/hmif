@extends('cms.layouts.app3')

@section('css')

@endsection

@section('scripts')

@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col s12">
                <div class="card">
                    <div class="card-content">
                        <a href="{{ URL::previous() }}" class="waves-effect waves-light btn-small"><i class="fa fa-arrow-circle-left mr-2"></i>Back</a>
                        <form action="{{route('post.destroy', ['post' => $post->id])}}" class="right" method="POST">
                            <a href="{{route('post.edit', ['post' => $post->id])}}" class="waves-effect waves-light btn-small"><i class="fa fa-pencil-square-o mr-2"></i>Edit Post</a>
                            <button type="submit" class="waves-effect waves-light btn-small"><i class="nav-icon fa fa-trash-o mr-2"></i>Trash</button>
                            @method('DELETE')
                            {{csrf_field()}}
                        </form>
                    </div>
                </div>
            </div>
            <div class="col s12">
                <div class="card">
                    <div class="card-image">
                        <img src="{{$post->featured}}" alt="{{$post->title}}" style="width:100%; height:auto;">
                        <span class="card-title">{{$post->title}}</span>
                    </div>
                    <div class="card-content">
                        <div class="container">
                            {!!$post->content!!}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
@endsection