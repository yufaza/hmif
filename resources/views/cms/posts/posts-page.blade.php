@extends('cms.layouts.app3')

@section('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href='{{asset('css/froala_editor.pkgd.min.css')}}' rel='stylesheet' type='text/css' />
<link href='{{asset('css/froala_style.min.css')}}' rel='stylesheet' type='text/css' />
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

@endsection

@section('js')

<script type='text/javascript' src='{{asset('js/froala_editor.pkgd.min.js')}}'></script>
<script>
    $(document).ready(function() {
        $('#summernote').froalaEditor();
    });
</script>
<script>
    $(document).ready(function() {
        $('#posts-table').DataTable({
            "scrollX": true,
            "lengthChange": false
        });
    } );
</script>
@endsection

@section('content')
<div class="row">
    <ul class="tabs">
        <li class="tab col s6"><a href="#test1">Post Lists</a></li>
        <li class="tab col s6"><a href="#test2">Add Post</a></li>
    </ul>
    <div id="test1" class="col s12">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <h3 class="header">List of Posts</h3>
                    <table id="posts-table">
                        <thead>
                            <tr>
                                <th>Featured Image</th>
                                <th>Title</th>
                                <th>Author</th>
                                <th>Tags</th>
                                <th>Creation Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($posts as $post)
                            <tr>
                            <td><img src="{{$post->featured}}" alt="{{$post->title}}" width="160px" height="90px"></td>
                            
                            <td>{{$post->title}}</td>
                            <td>{{$post->user->name}}</td>
                            <td>
                                @foreach($post->tags as $tag)
                                <span class="badge badge-info">{{$tag->tag." "}}</span>
                                @endforeach
                            </td>
                            <td>{{$post->created_at}}</td>
                            <td>
                                <form action="{{route('post.destroy', ['post' => $post->id])}}" method="POST">
                                @method('DELETE')
                                {{csrf_field()}}
                                <div class="btn-group-vertical w-100">
                                    <a href="{{route('post.show', ['post' => $post->id])}}" class="waves-effect waves-light btn-small col s12"><i class="material-icons left">cloud</i>View</a>
                                    <a href="{{route('post.edit', ['post' => $post->id])}}" class="waves-effect waves-light btn-small col s12"><i class="material-icons left">cloud</i>Edit</a>
                                    <button type="submit" class="waves-effect waves-light btn-small col s12"><i class="material-icons left">cloud</i></i>Trash</button>
                                </div>
                                </form>
                            </td>
                            </tr>
                            @endforeach
        
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Featured Image</th>
                                <th>Title</th>
                                <th>Author</th>
                                <th>Tags</th>
                                <th>Creation Date</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div id="test2" class="col s12">
        <div class="row">
            <div class="container">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <h3 class="header">Add New Post</h3>
                <form action="{{ route('post.store') }}" method="post" enctype="multipart/form-data">
                  {{ csrf_field() }}
                    <div class="input-field col s12">
                        {{-- <i class="material-icons prefix">textsms</i> --}}
                        <input type="text" name="title" id="title">
                        <label for="title">Title</label>
                    </div>
                    <div class="file-field input-field col s12">
                            <div class="btn">
                              <span>Featured Image</span>
                              <input type="file" name="featured" id="featured">
                            </div>
                            <div class="file-path-wrapper">
                              <input class="file-path validate" type="text">
                            </div>
                          </div>
                  <div class="input-field col s12">
                    <select name="category_id">
                        <option value="" disabled selected>Select a Category</option>
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                    <label>Category</label>
                    </div>
                    <div class="input-field col s12">
                        <p><label>
                            Tags
                        </label></p>
                        @foreach($tags as $tag)
                        <p>
                            <label>
                                <input type="checkbox"  class="filled-in" value="{{ $tag->id }}" name="tags[]">
                                <span>{{ $tag->tag }}</span> 
                            </label>
                        </p>
                        @endforeach
                    </div>

                    <div class="input-field col s12">
                        <textarea name="content" id="summernote">
                                &lt;p&gt;Here goes the initial content of the editor.&lt;/p&gt;
                        </textarea>
                        <label for="summernote">Content</label>
                    </div> 
                    <div class="input-field col s12">
                        <button class="btn waves-effect waves-light" type="submit" name="action">Submit
                            <i class="material-icons right">send</i>
                        </button>
                        <div>
                            <script async src="https://imgbb.com/upload.js" data-palette="blue" data-sibling-pos="before"></script>
                        </div>
                    </div>
                </form>

            </div>
        </div>
        
    </div>
</div>
@endsection