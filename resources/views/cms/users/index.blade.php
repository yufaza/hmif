@extends('cms.layouts.app3')

@section('css')

@endsection

@section('scripts')
<script>
  $(function () {
    $('#users-table').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>
@endsection

@section('content-header')
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v2</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
@endsection

@section('content')
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <a href="{{route('user.create')}}" class="btn btn-success"><i class="nav-icon fa fa-plus-square mr-2"></i>Add User</a>
              </div>
            </div>
          </div>
        </div> 
        <div class="row">
          <div class="col-lg-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">List of Users</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive">
              <table id="users-table" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Avatar</th>
                  <th>Name</th>
                  <th>Creation Date</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)

                <tr>
                  <td><img src="{{$user->profile->avatar}}" alt="{{$user->name}}" width="100px" height="100px"></td>
                  <td>{{$user->name}}</td>
                  <td>{{$user->created_at}}</td>
                  <td>
                    <form action="{{route('user.destroy', ['user' => $user->id])}}" method="POST">
                      @method('DELETE')
                      {{csrf_field()}}
                      <div class="btn-group-vertical w-100">
                        <a href="{{route('user.show', ['user' => $user->id])}}" class="btn btn-primary btn-sm"><i class="nav-icon fa fa-eye mr-2"></i>View</a>
                        @if(!$user->admin)
                          <a href="{{route('admin.promote', ['user' => $user->id])}}" class="btn btn-success btn-sm"><i class="nav-icon fa fa-angle-double-up mr-2"></i>Promote to Admin</a>
                        @else
                          @if(Auth::id() != $user->id)
                          <a href="{{route('admin.demote', ['user' => $user->id])}}" class="btn btn-danger btn-sm"><i class="nav-icon fa fa-minus-square mr-2"></i>Remove Privileges</a>
                          @endif
                        @endif
                        <button type="submit" class="btn btn-secondary btn-sm"><i class="nav-icon fa fa-trash-o mr-2"></i>Delete</button>
                      </div>
                    </form>
                  </td>
                </tr>
                @endforeach

                </tbody>
                <tfoot>
                <tr>
                  <th>Avatar</th>
                  <th>Name</th>
                  <th>Creation Date</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection