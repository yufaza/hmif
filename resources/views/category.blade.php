@extends('layouts.page-layout')

@section('styles')

@endsection

@section('scripts')

@endsection



@section('content')
  <div class="card">
    <div class="card-content">
      <div class="row">
        @foreach($categories as $category)
          <div class="col s12 m6">

              <div class="card text-left|center|right">
                  <div class="card-body">
                      <h4 class="card-title"><a href="{{route('single.category', ['slug' => $category->slug])}}">{{$category->name}}</a></h4>
                      <p class="card-text">{{$category->description}}</p>
                      @if($category->posts->last())
                        <div class="card-image">
                        <img class="card-img-top" src="/storage/{{$category->posts->last()->featured}}" alt="{{$category->posts->last()->title}}">
                      </div>
                        <p>{{$category->posts->last()->title}}</p>
                        {{ $category->posts->last()->created_at->diffForHumans() }}

                      @endif
                      <p class="card-text">Total Artikel : {{$category->posts->count()}}</p>
                  </div>
              </div>

          </div>
        @endforeach



      </div>
    </div>
  </div>
@endsection