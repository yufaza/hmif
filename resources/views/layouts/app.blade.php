<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <meta name="theme-color" content="#0091ea">
  <title>{{isset($title) ? $title : config('app.name')}}</title>
  @include('includes.css')
  @yield('styles')
</head>
<body>
  @include('includes.navigation')

  @include('includes.banner')

  @yield('main')

  @include('includes.parallax1')

  @yield('posts')

  @include('includes.parallax2')
 
  @include('includes.search')
  @include('includes.footer')

  @include('includes.scripts')
  @yield('scripts')
  </body>
</html>
