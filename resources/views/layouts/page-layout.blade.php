<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <meta name="theme-color" content="#0091ea">
  <title>{{isset($title) ? $title : config('app.name')}}</title>
  @include('includes.css')
  @yield('styles')
      <!-- 
                                                          .+sssssssssssssssssssso:                       
                                                      `/sssssssssssssssssssssssso-                     
                                          `:`        :.-osssssssssssssssssssssss+.                     
                                          :oss:`    -oss/.:++++++++++++++++++++/.                       
                                        :sssss/   -sssss+                                              
                                        :sssss+   -ssssso                                              
                                        :sssss+   -ssssso                                              
                                        :sssss+   -ssssso                                              
                                        :sssss+   -ssssso                                              
                                        :sssss+   -ssssso                                              
                                        :sssss+   -ssssso                                              
                                        :sssss+   -ssssso                                              
          ``..........````              :sssss+   -ssssso                        ``````````````        
      `-/++oooooooooooo+++//::-.```     :sssss+   -ssssso               ```...------::::::::::---.`    
    `/oooooooooooo+/-...```....--:::--.`:sssss:   .ossss/ `````````...-......````````.--:::::::::::-.  
    .ooooooooooooo:`                 ``..-/oso:`    ./so-./+++++++ooooo+++++++++-`      `-::::::::::::- 
    /oooooooooooo+                          ...--.``  .`:osssssssssssssssssssssss+-      `:::::::::::::`
    /ooooooooooooo                         `-`   `.---:-/ossssssssssssssssssssssso-      `:::::::::::::`
    -ooooooooooooo:                       .+so-`````-+so+/+soooooooooooooooooooo:`       -::::::::::::- 
    /ooooooooooooo-                     -sssss/`  -sssss+`.--.`                        -:::::::::::::` 
    `+ooooooooooooo/`                   /sssss+   -ssssso    .---`                   `-:::::::::::::.  
      /ooooooooooooo+-               `.`/sssss+   -ssssso       `.--.               .::::::::::::::.   
        -oooooooooooooo/.         `..``  :sssss+   -ssssso           .-:.`         .-:::::::::::::-`    
        `+oooooooooooooo/.    `..`      :sssss+   -ssssso              .::-`    `-::::::::::::::.      
          -ooooooooooooooo/....         :sssss+   -ssssso                 .:/..-::::::::::::::-`       
            :ooooooooooooooo+-          :sssss+   -ssssso                  `.:::::::::::::::-`         
              `:oooooooooooooooo:.       :sssss+   -ssssso                `-:::::::::::::::-`           
                `:oooooooooooooooo+-`    :sssss+   -ssssso             `.-:::::::::::::::-`             
                  `/+oooooooooooooooo/-` :sssss+   -ssssso          `.-::::::::::::::::/:`              
                `.-.`./ooooooooooooooooo/+sssss/   -sssss+       `.-::::::::::::::::-.`.+o:`            
              `-:-`    `-+ooooooooooooooooosss:`    .+ss:`    `.-:::::::::::::::::-`     -oo:`          
            `-:-.         .:+ooooooooooooooooo+:.`    -`  ``.-:::::::::::::::::-.`        `/oo:`        
          .-:-`            `./ooooooooooooooooooo/-.  `.--:::::::::::::::::-.`             -oo+-       
        `-::-`                `-/ooooooooooooooooooo++/:::::::::::::::::-.`                 -ooo/`     
        .::::`                    `-/+oooooooooooooooooooo+//:::::::::-.`                     /oooo-    
      `-:::::`                       `.:+oooooooooooooooooooooo+//:-.`                        -ooooo/`  
    `-::::::.                     ```.-::/+ooooooooooooooooooooooo+/:..`                    `+oooooo/` 
    -::::::::-.``        ````...---:::::::::/++oooooooooooooooooooooooo++/:--..```     ```.:+oooooooo/ 
    .::::::::::::-----------:::::::::::::::::::::/++ooooooooooooooooooooooooooooo+++++++++ooooooooooooo.
    -:::::::::::::::::::::::::::::::::::::::::::::----:/+oooooooooooooooooooooooooooooooooooooooooooooo/
    -::::::::::::::::::::::::::::::::::::::::---.``     `.-:++ooooooooooooooooooooooooooooooooooooooooo:
    `-:::::::::::::::::::::::::::::::::---..``               `.-:/+ooooooooooooooooooooooooooooooooooo+`
    `--::::::::::::::::::::::::----..``                           `.-:/+oooooooooooooooooooooooooooo/` 
      ``.-----::::::-------...``` ```    ``   ```    ```   ``   ``````` ``.-::/++oooooooooooooo++:-`   
            ``````````            /do   -dh   hdd/  :ddd`  hd.  ydhyyyy-         ```.........``        
                                  /my```-md   mmhd` hdmN`  dm-  hm/````                                
                                  /mdyyyhmd   mN/m+:m+dN`  dm-  hmhyyy+                                
                                  /my```:md   mN dddd`dN`  dm-  hm/...`                                
                                  /ms   -md   mm /mm+ dm`  dm-  hm:                                    
                                  ./-   `/:   :/  //` :/   :/`  :/.    
                                                BROTHERHOOD
  -->
</head>
<body>
  @include('includes.navigation')
  @include('includes.header')
  <div class="container" id="main-container">
    @yield('content')
  </div>
  @include('includes.search')

  @include('includes.footer')
  @include('includes.scripts')
  @yield('scripts')
  </body>
</html>
