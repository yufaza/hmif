@extends('layouts.search-layout')

@section('content')
  @include('includes.date_helper')<div class="row">
  @foreach($posts as $post)
  
    <div class="col s12 m4">
        <div class="card hoverable">
            <div class="card-image">
                <img src="/storage/{{$post->featured}}">
            </div>
            <div class="card-content">
                <span class="card-title truncate">{{$post->title}}</span>
                <p class="truncate" title="{{str_limit($post->content, 150)}}">{!!str_limit($post->content,
                    100)!!}</p>
            </div>
            <div class="card-action">
                <a href="{{route('post.single', ['slug' => $post->slug])}}">Lebih Detail..</a>
            </div>
        </div>
    </div>
  @endforeach</div>
  <?php //echo $posts->render(); 
  echo $posts->onEachSide(1)->links('includes.pagination-default');
  ?>

@endsection