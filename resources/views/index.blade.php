@extends('layouts.app')

  @section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/slick/slick.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/slick/slick-theme.css')}}"/>
    <style>
      .slick-slide {
          outline: none
      }
      #main-nav {
        transition: all 0.8s ease-out;
      }

      .transparent{
        box-shadow: 0 0 rgba(0,0,0,0);
      }

      .transparent a{
        font-weight:500;
        color: #ffffff;
      }

      .transparent .row a{
        font-weight:500;
        color: #ffffff!important;
      }

      .transparent .side-list a{
        font-weight:500;
        color: rgba(0,0,0,0.87)!important;
      }



      .height0{
        height:0px;
        padding:0px;
        margin:0px;
      }

    </style>
  @endsection

  @section('scripts')
    <script>
      $("#navigation").addClass("height0");
      $("#main-nav").addClass("nfixed");
      $("#main-nav").removeClass("white");
      $("#main-nav").addClass("transparent");
      $("#mobile-navigation").addClass("height0");
      $("#mobile-nav").addClass("nfixed");
      $("#mobile-nav").removeClass("white");
      $("#mobile-nav").addClass("transparent");
      $(window).scroll(function() {    
        var scroll = $(window).scrollTop();

        //>=, not <=
        if (scroll >= 65) {
            //clearHeader, not clearheader - caps H
            $("#main-nav").addClass("white");
            $("#main-nav").removeClass("transparent");
            $("#mobile-nav").addClass("white");
            $("#mobile-nav").removeClass("transparent");
        } else {
            $("#main-nav").removeClass("white");
            $("#main-nav").addClass("transparent");
            
            $("#mobile-nav").removeClass("white");
            $("#mobile-nav").addClass("transparent");
        }
      }); //missing );
    </script>
  @endsection

  @section('main')
<div class="container">
    <div class="section" id="main">

        <div class="row">
            <div class="col s12 center">
                <h3><i class="mdi-content-send brown-text"></i></h3>
                <h4 class="flow-text">Informasi terlengkap mengenai kegiatan mahasiswa Teknik Informatika.</h4>
            </div>
        </div>
        <div class="row">
            <div class="col s12 m12 l4">
                <div class="card hoverable">
                    <a href="{{route('post.categories')}}">
                        <div class="card-content center">
                            <i class="large material-icons light-green-text text-darken-2">ballot</i>
                            <span class="card-title black-text flow-text">Artikel</span>
                            <p class="truncate black-text">Artikel kami berdasarkan kategori</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col s12 m12 l4">
                <div class="card hoverable">
                    <a href="{{route('news.list')}}">
                        <div class="card-content center">
                            <i class="large material-icons blue-text text-darken-3">assignment</i>
                            <span class="card-title black-text flow-text">Berita</span>
                            <p class="black-text">Lihat Berita Terakhir</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col s12 m12 l4">
                <div class="card hoverable">
                    <a href="{{route('savings')}}">
                        <div class="card-content center">
                            <i class="large material-icons lime-text text-darken-1">account_balance</i>
                            <span class="card-title black-text flow-text">Tabungan Bersama</span>
                            <p class="black-text">Lihat Status Keuangan</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('posts')
<div class="container">
    <div class="section" id="article">

        <!--   Icon Section   -->

        <div class="row">
            <div class="col s8 offset-s2">
                <h4 class="header center light-blue-text text-accent-4">Artikel</h4>
                <h6 class="center-align">Informasi terbaru tentang kegiatan, prestasi dan artikel umum</h6>
            </div>
        </div>

        <div class="row posts-carousel">
            @foreach($posts as $post)
            <div class="col s12 m4">
                <div class="card hoverable">
                    <div class="card-image">
                        <img src="{{$post->featured}}">
                    </div>
                    <div class="card-content">
                        <span class="card-title truncate">{{$post->title}}</span>
                        <p class="truncate" title="{{str_limit($post->content, 150)}}">{{str_limit($post->content,
                            100)}}</p>
                    </div>
                    <div class="card-action">
                        <a href="{{route('post.single', ['slug' => $post->slug])}}">Lebih Detail..</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>

    </div>
</div>
  @endsection

