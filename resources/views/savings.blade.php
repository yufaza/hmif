@extends('layouts.page-layout')

@section('styles')
@endsection

@section('scripts')
@endsection



@section('content')
  <div class="card">
    <div class="card-content">
      <h2 class="hide-on-med-and-down center"><b>{{$title}}</b></h2>
      <h5 class="hide-on-large-only center"><b>{{$title}}</b></h5>
      @include('includes.date_helper')
    </div>
    <div class="card-content" style="text-align:justify">
      <div id="post-content">
        <?php 
          $total=0;
          $last = $savings->last();
        ?>
        @foreach($savings as $s)
          @if($s->debit == TRUE)
            <?php $total+=$s->amount?>
          @else
            <?php $total-=$s->amount?>
          @endif
        @endforeach
        <h3 class="center hide-on-med-and-down">Rp.<?=$total?></h3>
        <h5 class="center hide-on-large-only">Rp.<?=$total?></h5>
      </div>
    </div>
    <div class="card-content">
      <span class="small black-text"><i class="material-icons left">access_time</i>Terakhir diperbaharui pada: {{indonesian_date($latest->created_at)}}</span>
    </div>
  </div>
@endsection