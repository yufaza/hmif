@extends('layouts.search-layout')

@section('content')
  @include('includes.date_helper')
  @foreach($profile as $page)
    <div class="card ">
      <div class="card-content">
        <h3 class="hide-on-med-and-down center"><b>{{$page->title}}</b></h3>
        <h5 class="hide-on-large-only center"><b>{{$page->title}}</b></h5>
        <i class="material-icons left">access_time</i>{{indonesian_date($page->created_at)}} 
      </div>
    </div>
  @endforeach

@endsection