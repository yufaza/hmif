<footer class="page-footer light-blue accent-4">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text flow-text">Alamat</h5>
          <p class="grey-text text-lighten-4"><b>{{$settings->office}}</b></p>
          <p class="grey-text text-lighten-4">{{$settings->address}}</p>


        </div>
        <div class="col l3 s12">
          <h5 class="white-text flow-text">Kontak</h5>
          <ul>
            <li>HP: {{$settings->contact_number}}</li>
            <li><a class="white-text" href="mailto:{{$settings->contact_email}}">{{$settings->contact_email}}</a></li>
          </ul>
        </div>
        <div class="col l3 s12">
          <h5 class="white-text flow-text">Portal Terkait</h5>
          <ul>
            @foreach($settings->web_portals as $web_portal)
              <li><a class="white-text" target="blank" href="{{$web_portal->url}}">{{$web_portal->name}}</a></li>
            @endforeach
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      HMIF v.1.0.0 2018<a class="brown-text text-lighten-3" href="#"></a>
      </div>
    </div>
  </footer>