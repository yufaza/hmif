  <div class="parallax-container valign-wrapper">
    <div class="section no-pad-bot">
      <div class="container">
        <div class="row center">
          <h5 class="header col s12 light flow-text">Pantau terus kegiatan-kegiatan seru kami di Media sosial Resmi HMIF</h5>
          <a class="waves-effect waves-light btn deep-purple darken-4"><i class="material-icons left">camera_alt
</i>Instagram</a>
          <a class="waves-effect waves-light btn red darken-3"><i class="material-icons left">play_arrow</i>Youtube</a>
        </div>
      </div>
    </div>
    <div class="parallax"><img src="/storage/background_images/parallax-1.png" alt="HMIF"></div>
  </div>