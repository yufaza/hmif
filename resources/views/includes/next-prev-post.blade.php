<div class="row">
      @if($next) 
        <a href="{{route('post.single', ['slug' => $next->slug])}}" class="">
          <div class="col s6 left center">
            <div class="card hoverable" style="padding:5%">
              <div class="card-image">
                <img src="{{$next->featured}}" alt="">
              </div>
              <div class="card-action flow-text">
                << {{$next->title}}
              </div>
            </div>
          </div>
        </a>
      @endif
      @if($prev)
        <a href="{{route('post.single', ['slug' => $prev->slug])}}" class="">
          <div class="col s6 right center">
            <div class="card hoverable" style="padding:5%">
              <div class="card-image">
                <img src="{{$prev->featured}}" alt="">
              </div>
              <div class="card-action flow-text">
                {{$prev->title}} >>
              </div>
            </div>
          </div>
        </a>
      @endif
</div>