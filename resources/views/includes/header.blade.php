<header class="parallax-container valign-wrapper" id="header-post">
  <div class="parallax"><img src="/storage/background_images/secondary-background.png" alt="Unsplashed background img 2" class="glitch-img"></div>
  <div style="background:rgba(0,0,0,0.7);height:100%;width:100%;position:absolute"></div>
  <h5 class="animated lightSpeedIn delay-2 hide-on-large-only">{{$title}}</h5>
  <h3 class="animated lightSpeedIn delay-2 hide-on-med-and-down">{{$title}}</h3>

</header>
