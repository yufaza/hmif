  <div class="parallax-container valign-wrapper">
    <div class="section no-pad-bot">
      <div class="container">
        <div class="row center">
          <h5 class="header col s12 light flow-text">Mereka yang menghargai kebersamaan, tidak akan pernah ditinggalkan oleh kebersamaan itu sendiri.</h5>
        </div>
      </div>
    </div>
    <div class="parallax"><img src="/storage/background_images/parallax-2.png" alt="HMIF"></div>
  </div>