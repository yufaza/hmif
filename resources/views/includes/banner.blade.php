<header id="showcase" class="parallax-container valign-wrapper">
  <div class="parallax"><img src="/storage/background_images/main-background.png" alt="HMIF"></div>
  <div style="background:rgba(0,0,0,0.7);height:100%;width:100%;position:absolute"></div>
  <img src="/storage/background_images/logo-min.png" alt="HMIF" class="hide-on-med-and-down animated fadeIn delay-1 hmif-logo">
  <img src="/storage/background_images/logo-min.png" alt="HMIF" class="hide-on-large-only animated fadeIn delay-1 hmif-logo-small">
  <h3 class="animated lightSpeedIn delay-2 light flow-text">HIMPUNAN MAHASISWA TEKNIK INFORMATIKA</h3>
  <h5 class="animated fadeInUp delay-3 light flow-text">Universitas Muhammadiyah Kota Sukabumi.</h5>

</header>

