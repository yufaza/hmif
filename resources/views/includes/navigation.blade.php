<!--nav desktop -->
<div id="navigation" class="hide-on-med-and-down navbar-fixed">
 <!-- Dropdown Structure -->
  <ul id="profile" class="dropdown-content collection">
    @foreach($settings->page as $page)
      @if($page->type == 'profile')
      <li><a href="{{route('profile.single', ['slug' => $page->slug])}}" target="_blank">{{$page->title}}</a></li>
      @endif
    @endforeach
  </ul>
  <ul id="sosmed" class="dropdown-content collection">
    @foreach($settings->social_media as $social_media)
      <li><a href="{{$social_media->url}}" target="_blank">{{$social_media->name}}</a></li>
    @endforeach
  </ul>
  <ul id="category" class="dropdown-content collection">
    @foreach($categories as $category)
      <li><a href="{{route('single.category', ['slug' => $category->slug])}}">{{$category->name}}</a></li>
    @endforeach
  </ul>
  <nav id="main-nav" class="white" role="navigation">
    <div class="nav-wrapper container">
      <a href="/" class="brand-logo">
        <img src="/storage/background_images/logo-min.png" alt="HMIF" class="animated wobble hide-on-med-and-down" style="height:50px; padding-top:10px"/>
      </a>
     
      <ul class="right hide-on-med-and-down" id="nav-lists">
        <li><a class="dropdown-trigger" href="{{route('profile.list')}}" data-target="profile">Profil HMIF<i class="material-icons right">arrow_drop_down</i></a></li>
        <li><a class="dropdown-trigger" href="{{route('post.categories')}}" data-target="category">Artikel<i class="material-icons right">arrow_drop_down</i></a></li>
        <li><a class="dropdown-trigger" href="#!" data-target="sosmed">Media Sosial<i class="material-icons right">arrow_drop_down</i></a></li>
        <li><a href="{{route('news.list')}}">Berita</a></li>
        <li><a href="{{route('file.list')}}">Download</a></li>
        <li><a href="#" class="js-open-search"><i class="material-icons right">search</i></a></li>
    
      </ul>
    </div>
  </nav>
</div>
<!--nav mobile-->
<div id="mobile-navigation" class="navbar-fixed hide-on-large-only">
  <nav class="white" role="navigation" id="mobile-nav">
    <div class="nav-wrapper container">
      <ul id="nav-mobile" class="sidenav">
      <div class="center">
      <a href="{{route('index')}}"><img src="/storage/background_images/logo.png" alt="HMIF" class="hmif-logo-small" style="height:12vh; margin-top:5vh"/></a>
      </div>
        
        <li><a href="{{route('index')}}">Home</a></li>
        <li>
          <ul class="collapsible collapsible-accordion">
            <li>
              <a class="collapsible-header">Profile<i class="material-icons left">arrow_drop_down</i></a>
              <div class="collapsible-body">
                <ul>
                  @foreach($settings->page as $page)
                    @if($page->type == 'profile')
                    <li><a href="{{route('profile.single', ['slug' => $page->slug])}}" target="_blank">{{$page->title}}</a></li>
                    @endif
                  @endforeach
                </ul>
              </div>
            </li>
          </ul>
        </li>
        <li>
          <ul class="collapsible collapsible-accordion">
            <li>
              <a class="collapsible-header">Artikel<i class="material-icons left">arrow_drop_down</i></a>
              <div class="collapsible-body">
                <ul>
                  @foreach($categories as $category)
                    <li><a href="{{route('single.category', ['slug' => $category->slug])}}">{{$category->name}}</a></li>
                  @endforeach
                </ul>
              </div>
            </li>
          </ul>
        </li>
        <li>
          <ul class="collapsible collapsible-accordion">
            <li>
              <a class="collapsible-header">Media Sosial<i class="material-icons left">arrow_drop_down</i></a>
              <div class="collapsible-body">
                <ul>
                  @foreach($settings->social_media as $social_media)
                    <li><a href="{{$social_media->url}}" target="_blank">{{$social_media->name}}</a></li>
                  @endforeach
                </ul>
              </div>
            </li>
          </ul>
        </li>
        <li><a href="{{route('news.list')}}">Berita</a></li>
        <li><a href="{{route('file.list')}}">Download</a></li>
      </ul>
      <div class="row">
        <div class="col s2 left">
          <a href="#" data-target="nav-mobile" class="sidenav-trigger left"><i class="material-icons left">menu</i></a>
        </div>
        <div class="col s8">
          <a href="#" class="center truncate">{{$title}}</a>
        </div>
        <div class="col s2 right">
          <a href="#" class="js-open-search right"><i class="material-icons">search</i></a>
        </div>
      </div>
    </div>
  </nav>
</div>
