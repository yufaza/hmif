  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="{{asset('css/materialize.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="{{asset('css/style.css')}}" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">

  <style>
    #showcase{
      background-size:cover;
      background-position:center;
      height:calc(100vh - 0px);  
      display:flex;
      flex-direction:column;
      justify-content:center;
      align-items:center;
      text-align:center;
      padding:0 20px;
    }

    #showcase h1{
      font-size:50px;
      line-height:1.2;
    }

    #showcase p{
      font-size:20px;
    }

    #showcase .button{
      font-size:18px;
      text-decoration:none;
      color:#926239;
      border:#926239 1px solid;
      padding:10px 20px;
      border-radius:10px;
      margin-top:20px;
    }

    #showcase .button:hover{
      background:#926239;
      color:#fff;
    }

    .delay-1 {
     animation-delay: 2s;
    }

    .delay-2 {
        animation-delay: 2.2s;
    }

    .delay-3 {
        animation-delay: 2.4s;
    }

    .hmif-logo {
      height:40vh;
      width: auto;
      filter: drop-shadow(3px 3px 3px #222);
    }

    .hmif-logo-small {
      height:30vh;
      width: auto;
      filter: drop-shadow(3px 3px 2px #222);
    }

    .parallax-container h3, .parallax-container h5 {
      text-shadow: 1px 2px 2px #555555;
    }

    .nfixed {
      position:fixed;
    }

    #mobile-nav .row a {
        color: #0091ea;
    }


    #nav-list a {
      outline: none;
    }

    .dropdown-trigger{
      outline:none;
    }

    #header-post{
      background-size:cover;
      background-position:center;
      display:flex;
      flex-direction:column;
      justify-content:center;
      align-items:center;
      text-align:center;
      padding:0 20px;
    }


/*-----------------------------------------------------*/

  </style>