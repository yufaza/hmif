<div class="row">
      @if($next) 
        <a href="{{route('page.single', ['slug' => $next->slug])}}" class="">
          <div class="col s6 left center">
            <div class="card hoverable" style="padding:5%">
              <div class="card-content">
                <i class="material-icons left">arrow_back</i>{{$next->title}}
              </div>
            </div>
          </div>
        </a>
      @endif
      @if($prev)
        <a href="{{route('page.single', ['slug' => $prev->slug])}}" class="">
          <div class="col s6 right center">
            <div class="card hoverable" style="padding:5%">
              <div class="card-content">
                <i class="material-icons right">arrow_forward</i>{{$prev->title}}
              </div>
            </div>
          </div>
        </a>
      @endif
</div>