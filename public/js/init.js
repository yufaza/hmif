(function($){
  $(function(){

    $('.sidenav').sidenav();
    $('.parallax').parallax();
    $('.dropdown-trigger').dropdown({
      inDuration: 300,
      outDuration: 225,
      constrainWidth: true,
      hover: true, // Activate on hover
      coverTrigger: false,
      alignment: 'right' // Displays dropdown with edge aligned to the left of button
      }
    );
    $('.collapsible').collapsible();

  }); // end of document ready
})(jQuery); // end of jQuery name space
