//Global var
var CRUMINA = {};

(function ($) {

    // USE STRICT
    "use strict";

    //----------------------------------------------------/
    // Predefined Variables
    //----------------------------------------------------/
    var $window = $(window),
        $document = $(document),
        $body = $('body'),

        swipers = {},
        //Elements
        $header = $('#site-header'),
        $counter = $('.counter'),
        $progress_bar = $('.skills-item'),
        $pie_chart = $('.pie-chart'),
        $animatedIcons = $('.js-animate-icon'),
        $asidePanel = $('.right-menu'),
        $primaryMenu = $('.primary-menu'),
        $footer = $('#site-footer');



    var $popupSearch = jQuery(".popup-search");
    var $cartPopap = jQuery(".cart-popup-wrap");


    /* -----------------------
     * Fixed Header
     * --------------------- */

    CRUMINA.fixedHeader = function () {
        // grab an element
        $header.headroom(
            {
                "offset": 210,
                "tolerance": 5,
                "classes": {
                    "initial": "animated",
                    "pinned": "swingInX",
                    "unpinned": "swingOutX"
                }
            }
        );
    };

    /* -----------------------
     * Parallax footer
     * --------------------- */

    CRUMINA.parallaxFooter = function () {
        if ($footer.length && $footer.hasClass('js-fixed-footer')) {
            $footer.before('<div class="block-footer-height"></div>');
            $('.block-footer-height').matchHeight({
                target: $footer
            });
        }
    };

    /* -----------------------
     * COUNTER NUMBERS
     * --------------------- */
    CRUMINA.counters = function () {
        if ($counter.length) {
            $counter.each(function () {
                jQuery(this).waypoint(function () {
                    $(this.element).find('span').countTo();
                    this.destroy();
                }, {offset: '95%'});
            });
        }
    };

    /* -----------------------
     * Progress bars Animation
     * --------------------- */
    CRUMINA.progresBars = function () {
        if ($progress_bar.length) {
            $progress_bar.each(function () {
                jQuery(this).waypoint(function () {
                    $(this.element).find('.count-animate').countTo();
                    $(this.element).find('.skills-item-meter-active').fadeTo(300, 1).addClass('skills-animate');
                    this.destroy();
                }, {offset: '90%'});
            });
        }
    };

    /* -----------------------
     * Pie chart Animation
     * --------------------- */
    CRUMINA.pieCharts = function () {
        if ($pie_chart.length) {
            $pie_chart.each(function () {
                jQuery(this).waypoint(function () {
                    var current_cart = $(this.element);
                    var startColor = current_cart.data('start-color');
                    var endColor = current_cart.data('end-color');
                    var counter = current_cart.data('value') * 100;

                    current_cart.circleProgress({
                        thickness: 16,
                        size: 320,
                        startAngle: -Math.PI / 4 * 2,
                        emptyFill: '#fff',
                        lineCap: 'round',
                        fill: {
                            gradient: [startColor, endColor],
                            gradientAngle: Math.PI / 4
                        }
                    }).on('circle-animation-progress', function (event, progress) {
                        current_cart.find('.content').html(parseInt(counter * progress, 10) + '<span>%</span>'
                        )
                    }).on('circle-animation-end', function () {

                    });
                    this.destroy();

                }, {offset: '90%'});
            });
        }
    };
    /* -----------------------
     * Animate SVG Icons
     * --------------------- */
    CRUMINA.animateSvg = function () {
        if ($animatedIcons.length) {
            $animatedIcons.each(function () {
                jQuery(this).waypoint(function () {
                    var mySVG = $(this.element).find('> svg').drawsvg();
                    mySVG.drawsvg('animate');
                    this.destroy();
                }, {offset: '95%'});
            });
        }
    };

    /* -----------------------------
     * Toggle search overlay
     * ---------------------------*/
    CRUMINA.toggleSearch = function () {
        $body.toggleClass('open');
        $('.overlay_search-input').focus();
    };

    /* -----------------------------
     * Equal height
     * ---------------------------*/
    CRUMINA.equalHeight = function () {
        $('.js-equal-child').find('.theme-module').matchHeight({
            property: 'min-height'
        });
    };


    /* -----------------------------
     * On Click Functions
     * ---------------------------*/


    $window.keydown(function (eventObject) {
        if (eventObject.which == 27) {
            if ($asidePanel.hasClass('opened')) {
                CRUMINA.togglePanel();
            }
            if ($body.hasClass('open')) {
                CRUMINA.toggleSearch();
            }
        }
    });

    jQuery(".js-close-aside").on('click', function () {
        if ($asidePanel.hasClass('opened')) {
            CRUMINA.togglePanel();
        }
        return false;
    });

    jQuery(".js-open-aside").on('click', function () {
        if (!$asidePanel.hasClass('opened')) {
            CRUMINA.togglePanel();
        }
        return false;
    });
    jQuery(".js-open-search").on('click', function () {
        CRUMINA.toggleSearch();
        return false;
    });

    jQuery(".overlay_search-close").on('click', function () {
        $body.removeClass('open');
        return false;
    });

    jQuery(".js-open-p-search").on('click', function () {
        $popupSearch.fadeToggle();
    });

    if ($popupSearch.length) {
        $popupSearch.find('input').focus(function () {
            $popupSearch.stop().animate({
                'width': $popupSearch.closest('.container').width() + 70
            }, 600)
        }).blur(function () {
            $popupSearch.fadeToggle('fast', function () {
                $popupSearch.css({
                    'width': ''
                });
            });

        });
    }

    // Hide cart on click outside.
    $document.on('click', function (event) {
        if (!$(event.target).closest($cartPopap).length) {
            if ($cartPopap.hasClass('visible')) {
                $cartPopap.fadeToggle(200);
                $cartPopap.toggleClass('visible')
            }
        }
        if (!$(event.target).closest($asidePanel).length) {
            if ($asidePanel.hasClass('opened')) {
                CRUMINA.togglePanel();
            }
        }

    });

    // Show dropdown cart on icon click.
    jQuery(".js-cart-animate").on('click', function (event) {
        event.stopPropagation();
        $cartPopap.toggleClass('visible');
        $cartPopap.fadeToggle(200);
    });


    $('.quantity-plus').on('click', function () {
        var val = parseInt($(this).prev('input').val());
        $(this).prev('input').val(val + 1).change();
        return false;
    });

    $('.quantity-minus').on('click', function () {
        var val = parseInt($(this).next('input').val());
        if (val !== 1) {
            $(this).next('input').val(val - 1).change();
        }
        return false;
    });

    /*---------------------------------
     ACCORDION
     -----------------------------------*/
    jQuery('.accordion-heading').on('click', function () {
        jQuery(this).parents('.panel-heading').toggleClass('active');
        jQuery(this).parents('.accordion-panel').toggleClass('active');
    });

    //Scroll to top.
    jQuery('.back-to-top').on('click', function () {
        $('html,body').animate({
            scrollTop: 0
        }, 1200);
        return false;
    });

    jQuery(".input-inline").find('input').focus(function () {
        $(this).closest('form').addClass('input-drop-shadow');
    }).blur(function () {
        $(this).closest('form').removeClass('input-drop-shadow');
    });

    /* -----------------------------
     * On DOM ready functions
     * ---------------------------*/

    $document.ready(function () {

        if ($('#menu-icon-wrapper').length) {
            CRUMINA.burgerAnimation();
        }
        
    });
})(jQuery);