<?php

use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {
  $title = $faker->sentence(3);  
  return [
        'title' => $title,
        'slug' => str_slug($title),
        'content' => $faker->sentence(300),
        'category_id' => mt_rand(1, 2),
        'user_id' => 1,
        'featured' => '/storage/cover_images/noimage.png'
    ];
});
