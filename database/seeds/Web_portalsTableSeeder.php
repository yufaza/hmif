<?php

use Illuminate\Database\Seeder;

class Web_portalsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Web_portal::create([
            'name' => 'Universitas Muhammadiyah Sukabumi',
            'url' => 'ummi.ac.id'
         ]);
    }
}
