<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      App\Category::create([
        'name' =>  "Category 1",
        'slug' => "category-1",
        'description' => "First Category"
     ]);
     App\Category::create([
      'name' =>  "Category 2",
      'slug' => "category-2",
      'description' => "Second Category"
   ]);
    }
}

