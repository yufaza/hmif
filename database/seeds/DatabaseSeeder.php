<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(TagsTableSeeder::class);
        $this->call(Web_portalsTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(PostsTableSeeder::class);
        $this->call(PagesSeeder::class);

        App\Social_media::create([
            'name' => 'Instagram',
            'url' => 'https://instagram.com'
         ]);

        App\File::create([
            'name' => 'Contoh',
            'slug' => 'http://localhost/phpmyadmin/',
            'description' => 'contoh'
        ]);
        App\Image::create([
            'name' => 'Logo',
            'slug' => 'logo',
            'featured' => 'background_images/logo.png',
            'setting_id' => 1
        ]);
        App\Image::create([
            'name' => 'Logo Min',
            'slug' => 'logo-min',
            'featured' => 'background_images/logo-min.png',
            'setting_id' => 1
        ]);
        App\Image::create([
            'name' => 'Main Background',
            'slug' => 'main-background',
            'featured' => 'background_images/main-background.png',
            'setting_id' => 1
        ]);
        App\Image::create([
            'name' => 'Secondary Background',
            'slug' => 'secondary-background',
            'featured' => 'background_images/secondary-background.png',
            'setting_id' => 1
        ]);
        App\Image::create([
            'name' => 'Parallax 1',
            'slug' => 'parallax-1',
            'featured' => 'background_images/parallax-1.png',
            'setting_id' => 1
        ]);
        App\Image::create([
            'name' => 'Parallax 2',
            'slug' => 'parallax-2',
            'featured' => 'background_images/parallax-2.png',
            'setting_id' => 1
        ]);
    }
}
