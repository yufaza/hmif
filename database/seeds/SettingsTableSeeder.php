<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Setting::create([
           'site_name' =>  "laravel's Blog",
           'office' => "Kobong TI UMMI",
           'address' => "Edinburgh, Yokohama",
           'office' => "Kobong UMMI",
           'contact_number' => '8 900 8732 4438',
           'contact_email' => 'info@laravelblog.com'
        ]);
    }
}
