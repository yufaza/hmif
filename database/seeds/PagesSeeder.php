<?php

use Illuminate\Database\Seeder;

class PagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Page::create([
            'title' =>  "Visi dan Misi",
            'slug' => "visi-misi",
            'content' => "hahaha",
         ]);
         App\Page::create([
            'title' =>  "Susunan Kepengurusan",
            'slug' => "kepengurusan",
            'content' => "hahaha",
         ]);
         App\Page::create([
            'title' =>  "Tentang HMIF",
            'slug' => "tentang",
            'content' => "hahaha",
         ]);
         App\Page::create([
            'title' =>  "Website HMIF",
            'slug' => "website-hmif",
            'content' => "hahaha",
            'type' => 'news'
         ]);
         App\Page::create([
            'title' =>  "Pengurus Himpunan Priode 2003-2004",
            'slug' => "pengurus-himpunan-priode-2003-2004",
            'content' => "hahaha",
            'type' => 'past-organizer'
         ]);
    }
}
