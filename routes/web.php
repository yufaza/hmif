<?php
//web routes---------------------------------------------------
Route::get('/', [
    'uses' => 'FrontEndController@index',
    'as' => 'index'
]);

Route::get('/cms/dashboard', function(){
    $somePosts = \App\Post::where('title', 'like', '%'.request('query').'%')->paginate(6);
    return view('cms.dashboard')->with('posts', $somePosts)
                            ->with('title', 'Search: '.request('query'))
                            ->with('categories', \App\Category::all())
                            ->with('settings', \App\Setting::first())
                            ->with('query', request('query'));
});


Route::get('/article/{slug}', [
    'uses' => 'FrontEndController@singlePost',
    'as' => 'post.single'
]);

Route::get('/article', [
    'uses' => 'FrontEndController@postCategories',
    'as' => 'post.categories'
]);

Route::get('/category/{slug}', [
    'uses' => 'FrontEndController@singleCategory',
    'as' => 'single.category'
]);

Route::get('/news', [
    'uses' => 'FrontEndController@news',
    'as' => 'news.list'
]);

Route::get('/news/{slug}', [
    'uses' => 'FrontEndController@singleNews',
    'as' => 'page.single'
]);

Route::get('/profile', [
    'uses' => 'FrontEndController@profile',
    'as' => 'profile.list'
]);

Route::get('/profile/{slug}', [
    'uses' => 'FrontEndController@singleProfile',
    'as' => 'profile.single'
]);

Route::get('/download', [
    'uses' => 'FrontEndController@downloadPage',
    'as' => 'file.list'
]);

Route::get('/files/{file}', [
    'uses' => 'FrontEndController@downloadFile',
    'as' => 'file.download'
]);

Route::get('/savings', [
    'uses' => 'FrontEndController@savings',
    'as' => 'savings'
]);

Route::get('/results', function(){
    $somePosts = \App\Post::where('title', 'like', '%'.request('query').'%')->paginate(6);
    return view('results')->with('posts', $somePosts)
                            ->with('title', 'Search: '.request('query'))
                            ->with('categories', \App\Category::all())
                            ->with('settings', \App\Setting::first())
                            ->with('query', request('query'));
});



//CMS Routes----------------------------------------------------------
Auth::routes();
Route::group(['prefix' => 'cms', 'middleware' => 'auth'], function(){
    
    Route::get('/dashboard', function(){
        return view('cms.dashboard');
    });
    

    Route::get('/home', 'HomeController@index')->name('home');
    
    Route::resources([
        'post' => 'CMS\PostsController',
        'category' => 'CMS\CategoriesController',
        'tag' => 'CMS\TagsController',
        'user' => 'CMS\UsersController',
        'file' => 'CMS\FilesController',
        'image' => 'CMS\ImagesController',
        'page' => 'CMS\PagesController'
    ]);

    Route::get('settings', [
        'uses' => 'CMS\SettingsController@index',
        'as' => 'settings'
    ]);
    
    Route::get('posts/trashed', [
        'uses' => 'CMS\PostsController@trashed',
        'as' => 'post.trashed'
    ]);

    Route::get('/post/restore/{post}', [
        'uses' => 'CMS\PostsController@restore',
        'as' => 'post.restore'
    ]);   

    Route::get('/post/kill/{post}', [
        'uses' => 'CMS\PostsController@kill',
        'as' => 'post.kill'
    ]);   

    Route::get('/settings', [
        'uses' => 'CMS\SettingsController@index',
        'as' => 'settings'
    ]);

    Route::get('/user/profile', [
        'uses' => 'CMS\ProfilesController@index',
        'as' => 'user.profile'
    ]);

    Route::post('/user/profile/update', [
        'uses' => 'CMS\ProfilesController@update',
        'as' => 'user.profile.update'
    ]);

    Route::post('/settings/update', [
        'uses' => 'CMS\SettingsController@update',
        'as' => 'settings.update'
    ]);

    Route::get('/user/not-admin/{user}', [
        'uses' => 'CMS\UsersController@not_admin',
        'as' => 'admin.demote'
    ])->middleware('admin');

    Route::get('/user/admin/{user}', [
        'uses' => 'CMS\UsersController@admin',
        'as' => 'admin.promote'
    ])->middleware('admin');
    

});




