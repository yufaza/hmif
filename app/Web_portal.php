<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Web_portal extends Model
{
    protected $fillable = ['name', 'url'];

    public function settings(){
        return $this->belongsTo('App\Setting');
    }
}
