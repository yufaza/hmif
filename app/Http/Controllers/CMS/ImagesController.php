<?php

namespace App\Http\Controllers\CMS;

use App\Image;
use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cms.images.index')->with('images', Image::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cms.images.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'name' => 'required',
            'featured' => 'image|required|unique:images'
        ]);

        
        $image = new Image;
        
        if($request->hasFile('featured'))
        {
            $featured = $request->featured;
            $avatar_new_name = str_slug($request->name).'.png';
            $featured->storeAs('public/background_images', $avatar_new_name);

            $image_link = 'background_images/' . $avatar_new_name;
        }
        else
        {
            return redirect()->back();
        }

        $image->name = $request->name;
        $image->setting_id = 1;
        $image->featured = $image_link;
        $image->slug = str_slug($request->name);

        $image->save();

        Session::flash('success', 'You successfully created an image');
        return redirect()->route('image.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $image = Image::find($id);
        return view('cms.images.edit')->with('image', $image);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $this->validate($request, [
            'name' => 'required',
        ]);

        $image = Image::find($id);
        if($request->name != $image->name){
            Session::flash('success', 'You cannot change the name');
            return redirect()->route('image.index');
        }

        if($request->hasFile('featured'))
        {
            if(is_file(storage_path('app/public/' . $image->featured)))
            {
                unlink(storage_path('app/public/' . $image->featured));
            }
            
            $avatar = $request->featured;
            $avatar_new_name = str_slug($request->name).'.png';

            $avatar->storeAs('public/background_images/', $avatar_new_name);

            $image->featured = 'background_images/'.$avatar_new_name;
        } else {
            Session::flash('success', 'You successfully make an error');
            return redirect()->route('image.index');
        }
        $image->name = $request->name;
        $image->save();

        
        Session::flash('success', 'You successfully updated a file');
        return redirect()->route('image.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $image = Image::find($id);

        if(is_file(storage_path('app/public/' . $image->featured)))
        {
            unlink(storage_path('app/public/' . $image->featured));
        }

        $image->delete();
        
        Session::flash('success', 'You successfully deleted an image');
        return redirect()->back();
    }
}
