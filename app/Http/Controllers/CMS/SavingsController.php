<?php

namespace App\Http\Controllers\CMS;

use App\Saving;
use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SavingsController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('cms.settings.savings')->with('savings', Saving::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'amount' => 'required',
            'debit' => 'required',
            'description' => 'required'
        ]);
        $saving = new Saving;

        $saving->amount = $request->amount;
        $saving->debit = $request->debit;
        $saving->description = $request->description;
        $saving->save();

        Session::flash('success', 'You successfully added a savings data');
        return redirect()->route('saving.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $saving = Saving::find($id);
        return view('cms.settings.savings')->with('saving', $saving);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'amount' => 'required',
            'debit' => 'required',
            'description' => 'required'
        ]);
        $saving = Saving::find($id);

        $saving->amount = $request->amount;
        $saving->description = $request->description;
        $saving->debit = $request->debit;
        $category->save();

        
        Session::flash('success', 'You successfully updated a savings');
        return redirect()->route('saving.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $saving = Saving::find($id);

        $saving->delete();
        
        Session::flash('success', 'You successfully deleted a saving');
        return redirect()->back();

    }
}
