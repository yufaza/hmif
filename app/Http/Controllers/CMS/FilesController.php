<?php

namespace App\Http\Controllers\CMS;

use App\Category;
use App\File;
use Session;
use Illuminate\Support\Facades\Storage;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('cms.files.index')->with('files', File::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('cms.files.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'name' => 'required',
            'description' => 'required'
        ]);
        
        if($request->hasFile('featured'))
        {
            $featured = $request->featured;
            $featured_new_name = time().$featured->getClientOriginalName();
            $featured->storeAs('public/files', $featured_new_name);
   
            $file_link = 'files/'.$featured_new_name;
        } else {
            $file_link = $request->url;
        }

        $file = File::create([
            'name' => $request->name,
            'description' => $request->description,
            'featured' =>  $file_link,
            'slug' => str_slug($request->name),
        ]);

        Session::flash('success', 'You successfully created a file');
        return redirect()->route('file.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $file = File::find($id);
        return view('cms.files.edit')->with('file', $file);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required|max:191',
        ]);

        $file = File::find($id);

        if($request->hasFile('featured'))
        {
            if(is_file(storage_path('app/public/' . $file->featured)))
            {
                unlink(storage_path('app/public/' . $file->featured));
            }
            
            $avatar = $request->featured;
            $avatar_new_name = time().$avatar->getClientOriginalname();

            $avatar->storeAs('public/files', $avatar_new_name);

            $file->featured = 'files/'.$avatar_new_name;
        } else {
            $file->featured = $request->url;
        }
        $file->name = $request->name;
        $file->description =$request->description;


        $file->save();

        
        Session::flash('success', 'You successfully updated a file');
        return redirect()->route('file.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $file = file::find($id);

        if(is_file(storage_path('app/public/' . $file->featured)))
        {
            unlink(storage_path('app/public/' . $file->featured));
        }

        $file->delete();
        
        Session::flash('success', 'You successfully deleted a file');
        return redirect()->back();

    }
}
