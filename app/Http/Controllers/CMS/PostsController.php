<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Category;
use App\Post;
use App\Tag;
use App\Setting;
use Auth;
use Session;
class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $categories = Category::all();
        $tags = Tag::all();

        if($categories->count() == 0 || $tags->count() == 0)
        {
            Session::flash('info', 'You must have some categories and tags before attempting to create post');

            return redirect()->back();
        }
        return view('cms.posts.posts-page')->with('posts', Post::all())
                                            ->with('categories', $categories)
                                            ->with('tags', $tags);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::all();
        $tags = Tag::all();

        if($categories->count() == 0 || $tags->count() == 0)
        {
            Session::flash('info', 'You must have some categories and tags before attempting to create post');

            return redirect()->back();
        }

        return view('cms.posts.create')->with('categories', $categories)->with('tags', $tags);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:191',
            'featured' => 'image',
            'content' => 'required',
            'category_id' => 'required',
            'tags' => 'required'
        ]);
        
        if($request->hasFile('featured'))
        {
            $featured = $request->featured;
            $featured_new_name = time().$featured->getClientOriginalName();
            $featured->storeAs('public/cover_images', $featured_new_name);

            $image_link = '/storage/cover_images/' . $featured_new_name;
        }
        else
        {
            $image_link = '/storage/cover_images/noimage.png';
        }

        $post = Post::create([
            'title' => $request->title,
            'content' => $request->content,
            'featured' => $image_link,
            'category_id' => $request->category_id,
            'slug' => str_slug($request->title),
            'user_id' => Auth::id()
        ]);

        $post->tags()->attach($request->tags);

        Session::flash('success', 'Post created successfully');

        return redirect()->route('post.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::where('id', $id)->first();

        $next_id = Post::where('id', '>', $post->id)->min('id');
        $prev_id = Post::where('id', '<', $post->id)->max('id');

        return view('cms.posts.show')->with('post', $post)
            ->with('title', $post->title)
            ->with('settings', Setting::first())
            ->with('next', Post::find($next_id))
            ->with('prev', Post::find($prev_id))
            ->with('tags', Tag::all());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::withTrashed()->find($id);
        return view('cms.posts.edit')->with('post', $post)->with('categories', Category::all())->with('tags', Tag::all());
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {     
        $this->validate($request, [
            'title' => 'required|max:191|unique:posts',
            'content' => 'required',
            'category_id' => 'required'
        ]);

        $post = Post::withTrashed()->find($id);

        if($request->hasFile('featured'))
        {
            $featured = $request->featured;
            $featured_new_name = time().$featured->getClientOriginalName();
            $featured->storeAs('public/cover_images', $featured_new_name);

            $post->featured = '/storage/cover_images/' . $featured_new_name;
        }

        $post->title = $request->title;
        $post->slug = str_slug($request->title);
        $post->content = $request->content;
        $post->category_id = $request->category_id;

        $post->save();
        $post->tags()->sync($request->tags);

        Session::flash('success', 'Post updated successfully');
        return redirect()->route('post.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $post->delete();
        Session::flash('success', 'The post was just trashed.');

        return redirect()->route('post.index');
    }

    public function trashed(){
        $posts = Post::onlyTrashed()->get();

        return view('cms.posts.trashed')->with('posts', $posts);
    }

    public function restore($id)
    {
        $post = Post::withTrashed()->find($id);

        if($post->user && $post->category)
        {
            $post->restore();
            Session::flash('success', 'The post was just restored.');
            return redirect()->back();
        } else {
            Session::flash('success', 'The post must have an user and a category.');
            return redirect()->back();
        }

        Session::flash('success', 'The post was just restored.');
        return redirect()->back();
    }

    public function kill($id){
        $post = Post::withTrashed()->where('id', $id)->first();

        $uri_path = parse_url($post->featured, PHP_URL_PATH);
        $uri_segments = explode('/', $uri_path);

        if(is_file(storage_path('app/public/' . $uri_segments[2].'/'. $uri_segments[3])))
        {
            if($uri_segments[3] != 'noimage.png')
            {
                unlink(storage_path('app/public/' . $uri_segments[2].'/'. $uri_segments[3]));
            }
        }
        $post->forceDelete();

        Session::flash('success', 'Post deleted permanently.');
        return redirect()->back();
    }
}
