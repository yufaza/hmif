<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Setting;
use App\Category;
use App\Post;
use App\Tag;
use App\Page;
use App\File;
use App\Saving;


class FrontEndController extends Controller
{
    public function index()
    {
        return view('index')
            ->with('title', Setting::first()->site_name)
            ->with('categories', Category::all())
            ->with('posts', Post::take(6)->get())
            ->with('settings', Setting::first());
    }

    public function singlePost($slug)
    {
        $post = Post::where('slug', $slug)->first();

        $next_id = Post::where('id', '>', $post->id)->min('id');
        $prev_id = Post::where('id', '<', $post->id)->max('id');

        return view('post')->with('post', $post)
            ->with('title', $post->title)
            ->with('categories', Category::all())
            ->with('tags', Tag::all())
            ->with('posts', Post::orderBy('created_at', 'desc')->take(3)->get())
            ->with('settings', Setting::first())
            ->with('next', Post::find($next_id))
            ->with('prev', Post::find($prev_id));
    }

    public function singleCategory($slug)
    {
        $category = Category::where('slug', 'like', '%'.$slug.'%')->first();
        $somePosts = Post::where('category_id', $category->id)->paginate(6);
        return view('results')->with('posts', $somePosts)
                            ->with('title', 'Kategori: '.$category->name)
                            ->with('categories', \App\Category::all())
                            ->with('settings', \App\Setting::first());
    }

    public function test()
    {
        return view('post')
            ->with('title', Setting::first()->site_name)
            ->with('categories', Category::all())
            ->with('settings', Setting::first());
    }

    public function postCategories()
    {
        return view('category')
            ->with('title', 'Kategori')
            ->with('categories', Category::all())
            ->with('settings', Setting::first());
    }

    public function news()
    {
        $somePages = Page::where('type', 'news')->paginate(6);
        return view('news')
            ->with('title', 'Berita')
            ->with('news', $somePages)
            ->with('categories', Category::all())
            ->with('settings', Setting::first());
    }

    public function singleNews($slug)
    {
        $page = Page::where('slug', $slug)->first();

        $next_id = Page::where([['id', '>', $page->id], ['type', $page->type]])->min('id');
        $prev_id = Page::where([['id', '<', $page->id], ['type', $page->type]])->max('id');

        return view('page')->with('page', $page)
            ->with('title', $page->title)
            ->with('categories', Category::all())
            ->with('settings', Setting::first())
            ->with('next', Page::find($next_id))
            ->with('prev', Page::find($prev_id));
    }

    public function profile()
    {
        $somePages = Page::where('type', 'profile')->get();
        return view('profile')
            ->with('title', 'Profil')
            ->with('profile', $somePages)
            ->with('categories', Category::all())
            ->with('settings', Setting::first());
    }

    public function singleProfile($slug)
    {
        $page = Page::where('slug', $slug)->first();

        $next_id = Page::where([['id', '>', $page->id], ['type', $page->type]])->min('id');
        $prev_id = Page::where([['id', '<', $page->id], ['type', $page->type]])->max('id');

        return view('page')->with('page', $page)
            ->with('title', $page->title)
            ->with('categories', Category::all())
            ->with('settings', Setting::first())
            ->with('next', Page::find($next_id))
            ->with('prev', Page::find($prev_id));
    }

    public function downloadPage()
    {
        return view('file')
            ->with('files', File::paginate(10))
            ->with('title', 'Download File')
            ->with('categories', Category::all())
            ->with('settings', Setting::first()); 
    }

    public function downloadFile($file)
    {
        //dd($file);
        return Storage::download('public/files/'.$file);
    }

    public function savings()
    {
        return view('savings')->with('savings', Saving::all())
                                ->with('title', 'Tabungan Bersama')
                                ->with('latest', Saving::orderBy('created_at', 'desc')->first())
                                ->with('categories', Category::all())
                                ->with('settings', Setting::first()); 
    }


}
