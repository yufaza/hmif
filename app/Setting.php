<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = ['site_name', 'contact_number', 'contact_email', 'address'];

    public function web_portals()
    {
        return $this->hasMany('App\Web_portal');
    }

    public function social_media()
    {
        return $this->hasMany('App\Social_media');
    }
    public function page()
    {
        return $this->hasMany('App\Page');
    }
    public function image()
    {
        return $this->hasMany('App\Image');
    }
}

