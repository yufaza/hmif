<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    //
    protected $fillable = ['name', 'slug', 'featured'];

    public function setting(){
        return $this->belongsTo('App\Setting');
    }
}
