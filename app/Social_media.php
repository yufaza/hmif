<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Social_media extends Model
{
    protected $fillable = ['name', 'url'];

    public function setting(){
        return $this->belongsTo('App\Setting');
    }
}
