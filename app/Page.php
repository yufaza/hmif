<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = ['title', 'slug', 'content', 'type'];

    public function setting(){
        return $this->belongsTo('App\Setting');
    }
}
